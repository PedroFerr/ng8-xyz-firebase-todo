/* tslint:disable:no-unused-variable */
import { async, inject, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA, EventEmitter } from '@angular/core';

// Need to import ReactiveFormsModule, as we did for 'projects\ng-xyz-ui-lib\src\lib\ng-xyz-ui-lib.module.ts',
// to have FormGroup, FormBuilder and Validators to work on the AuthFormComponent:
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { FormValues } from '../ng-xyz-ui-lib.interfaces';

import { AuthFormComponent } from './auth-form.component';

describe('AuthFormComponent', () => {
    let thisComponent: AuthFormComponent;
    let fixture: ComponentFixture<AuthFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AuthFormComponent],
            imports: [ReactiveFormsModule],
            providers: [FormBuilder],
            schemas: [NO_ERRORS_SCHEMA]
        })
        .compileComponents()
        .then(() => {            // => equivalent to 'beforeEach(() => {}),' OUTSIDE this 1st async beforeEach(() => {})
            fixture = TestBed.createComponent(AuthFormComponent);
            thisComponent = fixture.componentInstance;
            fixture.detectChanges();
        });
    }));

    // Check if component's ReactiveForm is, in fact, updating when inputted values are valid, and reacting to invalid ones
    describe('Should ReactiveForm "userForm" UPDATE when VALID inputs are validated, and be INVALID otherwise:', () => {
        // Create acommonly used function for a dry spec:
        const updateForm = (userEmail: string, userPassword: string, userName: string = null) => {
            if (thisComponent.userForm.controls.name) { thisComponent.userForm.controls.name.setValue(userName); }
            thisComponent.userForm.controls.email.setValue(userEmail);
            thisComponent.userForm.controls.password.setValue(userPassword);
        };
        const validUser = { name: 'Pedro', email: 'PedroFerr@Hotmail.com', password: '123456789a'};
        // tslint:disable-next-line: variable-name
        const invalidUser_1 = { email: 'PedroFerr@@@Hotmail.com', password: ' ' };
        // tslint:disable-next-line: variable-name
        const invalidUser_2 = { email: 'PedroFerr@Hotmail.com', password: ' ' };
        // tslint:disable-next-line: variable-name
        const invalidUser_3 = { email: 'PedroFerr@@@Hotmail.com', password: '123456789a' };

        it('"userForm" should exist', () => {
            expect(thisComponent.userForm).toBeDefined();
        });

        // Get in "that" last position branch, on the component's TS 'onValueChanged()',! ;-) Sun of a gun...
        it('"userForm" should have error validation messages, for each invalid input', () => {
            const fields = ['name', 'email', 'password'];

            fields.forEach(field => {
                const controlPerInput  = thisComponent.userForm.get(field);
                const messagesPerInput = thisComponent.validationMessages[field];

                expect(thisComponent.validationMessages[field]).not.toBeUndefined();
                expect(thisComponent.formErrors[field]).not.toBeUndefined();
                if (controlPerInput && controlPerInput.dirty && !controlPerInput.valid) {
                    expect(thisComponent.formErrors[field]).toEqual(thisComponent.validationMessages[field]);
                }
            });

            // It's local variables - 'control', 'messages' );
            // you'll never be able to get there, if they don't belong as properties from 'thisComponnet'...
            // So only by changing component's code you would reach 100% of coverage on this 'auth-form' component.

        });

        it('"userForm" should update from ReactiveForm CHANGES', () => {
            updateForm(validUser.email, validUser.password, validUser.name);
            expect(thisComponent.userForm.value).toEqual(validUser);
        });

        it('"userForm"\'s "valid" is FALSE when ReactiveForm is invalid - tested for 3 cases of invalid mail/password', () => {
            updateForm(invalidUser_1.email, invalidUser_1.password);
            expect(thisComponent.userForm.valid).toBeFalsy();

            updateForm(invalidUser_2.email, invalidUser_2.password);
            expect(thisComponent.userForm.valid).toBeFalsy();

            updateForm(invalidUser_3.email, invalidUser_3.password);
            expect(thisComponent.userForm.valid).toBeFalsy();

            // Why not...?
            updateForm(validUser.email, validUser.password);
            expect(thisComponent.userForm.valid).toBeTruthy();
            // Go to the extreme...?
            updateForm(null, null);
            expect(thisComponent.userForm.valid).toBeFalsy();
            updateForm(undefined, undefined);
            expect(thisComponent.userForm.valid).toBeFalsy();


        });

    });

    // Test the creation of the Component under current Unit Testing:
    it('Should create AuthFormComponent, using ReactiveFormsModule', () => {
        expect(thisComponent).toBeTruthy();
    });

    // Test 'x%' of the Component's METHODs, so test coverage will reach 'x%', on 'Functions()' column.
    describe('Should AuthFormComponent METHODs do what\'s expected, on: ', () => {
        const methodsToTest: Array<string> = ['toggleForm', 'signup', 'login', 'resetPassword'];

        for (const thisMethod of methodsToTest) {

            const itString = thisMethod !== 'resetPassword' ?
                `${thisMethod}() method: should emit back the "userForm" value (a \'validUser\')`
                :
                `${thisMethod}() method: should emit back a simple boolean set to true`
            ;

            switch (thisMethod) {
                case 'toggleForm':
                    it(`${thisMethod}() method: should toggle \'isNewUser\' flag (for sign up/in)`, () => {
                        const previousIsNewUser: boolean = thisComponent.isNewUser;
                        const methodCall = thisComponent.toggleForm();
                        // And now:
                        expect(thisComponent.isNewUser).toBe(!previousIsNewUser);
                    });
                    break;

                case 'signup':
                case 'login':
                case 'resetPassword':
                    it(itString, () => {
                        let componentEmitter: EventEmitter<boolean | FormValues>;
                        if (thisMethod === 'signup') { componentEmitter = thisComponent.userHasSignUp; }
                        if (thisMethod === 'login') { componentEmitter = thisComponent.userHasSignIn; }
                        if (thisMethod === 'resetPassword') { componentEmitter = thisComponent.userWantsToResetPassword; }

                        let formValuesToEmitBack: FormValues;
                        const validUser = { name: 'Pedro', email: 'PedroFerr@Hotmail.com', password: '123456789a'};
                        let aBooleanToEmitBack: boolean;
                        if (thisMethod === 'resetPassword') {
                            componentEmitter.subscribe((changes: EventEmitter<boolean>) => aBooleanToEmitBack = true);
                            // Check set up is correct:
                            expect(aBooleanToEmitBack).toBeUndefined();
                        } else {
                            // Boolean is easy... but let's inject some valid user and see what we'll have
                            // once 'componentEmitter' (either 'thisComponent.userHasSignUp' or 'thisComponent.userHasSignIn') CHANGEs
                            componentEmitter.subscribe((changes: EventEmitter<FormValues>) => formValuesToEmitBack = validUser);
                            // Check set up is correct:
                            expect(formValuesToEmitBack).toBeUndefined();
                        }

                        // Call component's method:
                        // ------------------------------------------------------------------------
                        // const compMethodCall = thisComponent[`${thisMethod}()`];    // <= Will NOT trigger each method!
                        if (thisMethod === 'signup') { thisComponent.signup(); }
                        if (thisMethod === 'login') { thisComponent.login(); }
                        if (thisMethod === 'resetPassword') { thisComponent.resetPassword(); }
                        // ------------------------------------------------------------------------

                        // See what happened to each '@Output() [componentEmitter]':
                        if (thisMethod === 'resetPassword') {
                            expect(aBooleanToEmitBack).toBe(true);
                        } else {
                            expect(formValuesToEmitBack).toBe(validUser);
                        }
                    });
                    break;

                default:
                    break;
            }
        }

        // MIND YOU:
        // A coverage of 86.84%, special if the left 13% belong exclusively to 'Branches' (52.63%)
        // and 'Lines' (84.38%, on which 27 are covered out of 32) is not bad at all...
        // but, that's a fact, could be eactly the area where the whole App could be japardize, because of a "bad logic" branch...
        // So... let's walk to the 100% coverage!
        //
        // // 1st test if !this.userForm:
        // thisComponent.userForm = null;
        // // tslint:disable-next-line: max-line-length
        // // Now call the PRIVATE method:
        // // @ts-ignore
        // thisComponent.onValueChanged();
        // // and the "if (this.userForm) {}" branch should not have the "E" flag noe, meaning the "else" was also tested!

        // // 2nd one @ "if ( this.formErrors.hasOwnProperty(field) && (field === 'name' ... ) {}"
        // delete thisComponent.formErrors.email;
        // delete thisComponent.formErrors.name;
        // delete thisComponent.formErrors.password;
        // // @ts-ignore
        // thisComponent.onValueChanged();
        // // And App still runs and Jasmine Test should have the same % - branches coverage is, though, increased

        // // 3rd one: "if (control && control.dirty && !control.valid) {}"

        it('should PRIVATE onValueChanged() NOT break even if "userForm" is null', () => {
            thisComponent.userForm = null;
            // Now call this PRIVATE method, again:
            // @ts-ignore
            thisComponent.onValueChanged();
            // Expect something:
            expect(thisComponent.userForm).toBeNull();
        });
        it('should PRIVATE onValueChanged() NOT break even if "validationMessages" is null', () => {
            // thisComponent.validationMessages = null;
            // @ts-ignore
            thisComponent.onValueChanged();

            // Do some demages:
            // delete thisComponent.userForm.controls.email;
            // delete thisComponent.formErrors.name;
            // delete thisComponent.formErrors.email;
            // delete thisComponent.userForm.controls.email;
            // delete thisComponent.formErrors.password;
            // thisComponent.formErrors['cabum'] = 'Ahah!';
            // Produce a change:
            // thisComponent.userForm.controls.email.setValue('High!');
            // thisComponent.userForm.controls.name.setValue('Hi!');


            // thisComponent.validationMessages = null;
            // // @ts-ignore
            // thisComponent.onValueChanged();

            // thisComponent.userForm.controls.email.setValue('High!');

            // const control = thisComponent.userForm.get('email');
            // control.markAsDirty();

            // // delete thisComponent.formErrors.name;
            // // delete thisComponent.formErrors.password;
            // // Produce a change:
            // // thisComponent.userForm.controls.email.setValue('High!');
            // // Expect something:
            // expect(thisComponent.validationMessages).toBeNull();

            expect(thisComponent.validationMessages).not.toBeNull();


        });


    });


});
