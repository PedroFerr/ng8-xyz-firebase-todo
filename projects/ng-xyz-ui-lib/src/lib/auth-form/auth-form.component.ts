import { Component, OnInit, OnDestroy, Output, EventEmitter, Input, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Subscription } from 'rxjs';

import { FormValues, FormErrors, FormValidators } from '../ng-xyz-ui-lib.interfaces';

@Component({
    selector: 'xyz-auth-form',
    templateUrl: './auth-form.component.html',
    styleUrls: ['./auth-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthFormComponent implements OnInit, OnDestroy {

    isNewUser = false; // to toggle, 1st, login or signup form

    userForm: FormGroup;
    formValueChangesSubscription: Subscription;

    userFormValues: FormValues;
    formErrors: FormErrors;
    validationMessages: FormValidators;

    @Output() userHasSignUp = new EventEmitter<FormValues>();
    @Output() userHasSignIn = new EventEmitter<FormValues>();

    @Output() userWantsToResetPassword = new EventEmitter<FormValues>();
    @Input() passwordHasBeenReset = false;	// set to true when password reset is triggered, on the host component

    constructor(private fb: FormBuilder) { }

    ngOnInit() {
        this.buildForm();
        this.initFormErrors();
        this.initFormValidators();
    }

    ngOnDestroy() {
        if (this.formValueChangesSubscription) {
            this.formValueChangesSubscription.unsubscribe();
        }
    }

    private buildForm() {
        this.userForm = this.fb.group(
            {
                name: [ '', [ Validators.minLength(4), Validators.maxLength(20) ] ],
                email: ['', [ Validators.required, Validators.email ] ],
                password: ['', [
                        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
                        Validators.minLength(6),
                        Validators.maxLength(15)
                    ]
                ]
            }
        );

        // Start listening to form value changes:
        this.formValueChangesSubscription = this.userForm.valueChanges.subscribe((data) => this.onValueChanged(data));
        // reset validation messages
        this.onValueChanged();
    }

    private onValueChanged(data?: any) {
        if (this.userForm) {

            for (const field in this.formErrors) {
                if ( this.formErrors.hasOwnProperty(field) && (field === 'name' || field === 'email' || field === 'password') ) {
                    this.formErrors[field] = '';

                    const control = this.userForm.get(field);
                    if (control && control.dirty && !control.valid) {
                        const messages = this.validationMessages[field];

                        if (control.errors) {
                            for (const key in control.errors) {
                                if (control.errors.hasOwnProperty(key)) {
                                    this.formErrors[field] += `${(messages as { [key: string]: string })[key]} `;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private initFormErrors() {
        this.formErrors = {
            name: '',
            email: '',
            password: ''
        };
    }

    private initFormValidators() {
        this.validationMessages = {
            name: {
                required: 'Name is required.'
                , minlength: 'Name must be at least 4 characters long.'
                , maxlength: 'Name cannot be more than 20 characters long.'
            }
            , email: {
                required: 'Email is required.'
                , email: 'Email must be a valid email'
            }
            , password: {
                required: 'Password is required.'
                , pattern: 'Password must include at least one letter and one number.'
                , minlength: 'Password must be at least 6 characters long.'
                , maxlength: 'Password cannot be more than 15 characters long.'
            }
        };
    }

    // ===========================================================

    public toggleForm() {
        this.isNewUser = !this.isNewUser;
    }

    public signup() {
        this.userHasSignUp.emit(this.userForm.value);
    }

    public login() {
        this.userHasSignIn.emit(this.userForm.value);
    }

    public resetPassword() {
        this.userWantsToResetPassword.emit(this.userForm.value);
    }

}
