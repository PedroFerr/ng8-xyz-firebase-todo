import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { NgXyzUiLibComponent } from './ng-xyz-ui-lib.component';

import { SubmitButtonComponent } from './submit-button/submit-button.component';
import { AuthFormComponent } from './auth-form/auth-form.component';
import { UserCardComponent } from './user-card/user-card.component';
import { ButtonsFlexBarComponent } from './buttons-flex-bar/buttons-flex-bar.component';
import { TodoFormCreateComponent } from './todo-forms/todo-form-create/todo-form-create.component';
import { TodoFormEditComponent } from './todo-forms/todo-form-edit/todo-form-edit.component';

@NgModule({
    imports: [
        // to enable Ng Directives like 'ngFor', '*ngIf', etc. on Ng Component's HTML templates
        CommonModule,
        // to enable any kind of Ng Forms manipulation
        ReactiveFormsModule, FormsModule,
        // to enable any kind of Router Directives inside this Module's Component's templates (router-outlet, routerLink, routerLinkActive)
        RouterModule
    ],

    // tslint:disable:max-line-length
    declarations: [
        NgXyzUiLibComponent,

        SubmitButtonComponent,
        AuthFormComponent,
        UserCardComponent,
        ButtonsFlexBarComponent,
        TodoFormCreateComponent,
        TodoFormEditComponent

    ],

    exports: [
        NgXyzUiLibComponent,

        SubmitButtonComponent,
        AuthFormComponent,
        UserCardComponent,
        ButtonsFlexBarComponent,
        TodoFormCreateComponent,
        TodoFormEditComponent
    ]
})
export class NgXyzUiLibModule { }
