export interface User {
    uid: string;
    email: string;
    displayName: string;
    photoURL: string;
    emailVerified: boolean;
    loginSource: string;
}

export type UserFormKeys = 'name' | 'email' | 'password';

export type FormValues = { [key in UserFormKeys]: string };
export type FormErrors = { [key in UserFormKeys]: string };
export type FormValidators = { [key in UserFormKeys]: object };

export interface Item {
    todo: string;
    done: boolean;
    userId: string;
}
// And then we'll need the AUTO ID (internal UID of each created collection's Object - a document - so it can be manipulated later):
export interface ItemWithId extends Item { autoID: string; }

