import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'xyz-ng-xyz-ui-lib',
  template: `
    <p>
      ng-xyz-ui-lib works!
    </p>
  `,
  styles: []
})
export class NgXyzUiLibComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
