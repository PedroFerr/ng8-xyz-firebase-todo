import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgXyzUiLibComponent } from './ng-xyz-ui-lib.component';

describe('NgXyzUiLibComponent', () => {
  let component: NgXyzUiLibComponent;
  let fixture: ComponentFixture<NgXyzUiLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgXyzUiLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgXyzUiLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
