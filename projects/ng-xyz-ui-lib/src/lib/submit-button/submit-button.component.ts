import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'xyz-submit-button',
    templateUrl: './submit-button.component.html',
    styleUrls: ['./submit-button.component.scss'],
    // Next obliges us to work with immutable objects (Observables), or manually trigger (by DOM event or Method) a changes detection.
    // (the component only depends on its @Inputs() initialization/changes - will never changed, once initialized, on any other vars)
    changeDetection: ChangeDetectionStrategy.OnPush
    // Check it here: https://netbasal.com/a-comprehensive-guide-to-angular-onpush-change-detection-strategy-5bac493074a4
})
export class SubmitButtonComponent implements OnInit {

    @Input() isFullWidth = false;
    @Input() isLabelUppercase = true;
    @Input() isButtonToDisabled = false;
    @Input() isIconToActivate = false;

    constructor() { }

    ngOnInit() {
    }

}
