import { TestBed } from '@angular/core/testing';

import { NgXyzUiLibService } from './ng-xyz-ui-lib.service';

describe('NgXyzUiLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgXyzUiLibService = TestBed.get(NgXyzUiLibService);
    expect(service).toBeTruthy();
  });
});
