import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'xyz-buttons-flex-bar',
    templateUrl: './buttons-flex-bar.component.html',
    styleUrls: ['./buttons-flex-bar.component.scss']
})
export class ButtonsFlexBarComponent implements OnInit {

    @Input() buttonsDisabled = [];

    @Output() toggleMenu = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

}
