import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../ng-xyz-ui-lib.interfaces';

@Component({
    selector: 'xyz-user-card',
    templateUrl: './user-card.component.html',
    styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {

    @Input() user: User;

    @Output() goToProfile = new EventEmitter();
    @Output() loggout = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

}
