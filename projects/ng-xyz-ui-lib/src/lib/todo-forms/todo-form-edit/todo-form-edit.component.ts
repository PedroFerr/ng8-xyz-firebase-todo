import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { ItemWithId } from '../../ng-xyz-ui-lib.interfaces';

@Component({
    selector: 'xyz-todo-form-edit',
    templateUrl: './todo-form-edit.component.html',
    styleUrls: ['../_common/todo-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoFormEditComponent implements OnInit {

    isEditing = false;
    componentDOM: HTMLElement;

    todoItem: ItemWithId;
    todoText: string;
    itemTodoManipulated: ItemWithId;

    @Input() itemTodo: ItemWithId;

    @Output() manipulatedTodo = new EventEmitter<
        { action: string; item: ItemWithId; propToUpdate: { [key: string]: string | boolean } }
    >();

    constructor() { }

    ngOnInit() {
    }

    public userIsTyping(evt: KeyboardEvent) {
        const textArea = evt.target as any;
        this.todoText = textArea.value;
    }

    public editTodoOnText() {
        this.manipulatedTodo.emit(
            {
                action: 'update',
                item: Object.assign( {}, this.itemTodo, { todo: this.todoText}),
                propToUpdate: { todo: this.todoText}
            }
        );
    }

    public makeTodoDone() {
        // if (this.itemTodo) {
            const newDone = this.itemTodo !== undefined ? !this.itemTodo.done : false;
            this.manipulatedTodo.emit(
                {
                    action: 'update',
                    item: Object.assign( {}, this.itemTodo, { done: newDone }),
                    propToUpdate: { done: newDone }
                }
            );
        // }
    }

    public deleteTodo() {
        window.confirm('The traditional "Are you sure....?"') ?
            this.manipulatedTodo.emit(
                { action: 'delete', item: this.itemTodo, propToUpdate: null}
            )
            :
            // tslint:disable-next-line: no-unused-expression
            false
        ;
    }
}
