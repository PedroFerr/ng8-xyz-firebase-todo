/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, EventEmitter } from '@angular/core';

import { TodoFormEditComponent } from './todo-form-edit.component';

import { ItemWithId } from '../../ng-xyz-ui-lib.interfaces';

describe('TodoFormEditComponent', () => {
    let thisComponent: TodoFormEditComponent;
    let fixture: ComponentFixture<TodoFormEditComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TodoFormEditComponent]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(TodoFormEditComponent);
            thisComponent = fixture.componentInstance;
            fixture.detectChanges();
        });
    }));

    it('Should create TodoFormEditComponent', () => {
        expect(thisComponent).toBeTruthy();
    });

    // Test 'x%' of the Component's METHODs, so test coverage will reach 'x%', on 'Functions()' column.
    describe('Should TodoFormEditComponent METHODs do what\'s expected, on: ', () => {
        const methodsToTest: Array<string> = ['userIsTyping', 'editTodoOnText', 'makeTodoDone', 'deleteTodo'];

        for (const thisMethod of methodsToTest) {
            const itString = thisMethod === 'editTodoOnText' ?
                `${thisMethod}() method: should emit back the edited TODO obj - propToUpdate: { todo: this.todoText}, action: 'update'`
                :
                thisMethod === 'makeTodoDone' ?
                    `${thisMethod}() method: should emit back the edited TODO obj - propToUpdate: { done: !itemTodo.done}, action: 'update'`
                    :
                    // thisMethod === 'deleteTodo'
                    // tslint:disable-next-line: max-line-length
                    `${thisMethod}() method: should emit back the "to-delete" TODO obj - propToUpdate: null, action 'delete'; fake window.confirm to always be TRUE`
            ;

            switch (thisMethod) {
                case 'userIsTyping':
                    it(`${thisMethod}() method: should equal 'this.todoText\' to the typed text`, () => {
                        expect(thisComponent.todoText).toBeUndefined();
                        // Trigger the method that reads waht user is typing
                        thisComponent.userIsTyping( {target: { value: 'something'}} as unknown as KeyboardEvent );
                        // See if wht's typed is in 'this.todoText':
                        expect(thisComponent.todoText).toBe('something');
                    });
                    break;

                case 'editTodoOnText':
                case 'makeTodoDone':
                case 'deleteTodo':
                    it(itString , async(() => {
                        interface TodoObjToEdit {
                            action: string;
                            item: ItemWithId;
                            propToUpdate: { [key: string]: string | boolean };
                        }
                        const componentEmitter: EventEmitter<TodoObjToEdit> = thisComponent.manipulatedTodo;
                        let editedTodoToEmitBack: TodoObjToEdit;

                        thisComponent.todoText = 'the edited TODO text';    // for whatever means the application has collect it.
                        // tslint:disable-next-line: max-line-length
                        const commonTodoItem: ItemWithId = { todo: 'TODO text', done: false, userId: 'someUserId123', autoID: 'I didn\'t made it!' };
                        const someEditedTodo: TodoObjToEdit = thisMethod === 'editTodoOnText' ?
                            { action: 'update', item: commonTodoItem, propToUpdate: { todo: thisComponent.todoText} }
                            :
                            thisMethod === 'makeTodoDone' ?
                                { action: 'update', item: commonTodoItem, propToUpdate: { done: true} }
                                :
                                // thisMethod === 'deleteTodo'
                                { action: 'delete', item: commonTodoItem, propToUpdate: null }
                        ;

                        componentEmitter.subscribe((changes: EventEmitter<TodoObjToEdit>) => editedTodoToEmitBack = someEditedTodo);
                        // Check set up is correct:
                        expect(editedTodoToEmitBack).toBeUndefined();

                        // Call component's method:
                        // ------------------------------------------------------------------------
                        // const compMethodCall = thisComponent[`${thisMethod}()`];    // <= Will NOT trigger each method!
                        if (thisMethod === 'editTodoOnText') { thisComponent.editTodoOnText(); }
                        if (thisMethod === 'makeTodoDone') { thisComponent.makeTodoDone(); }
                        if (thisMethod === 'deleteTodo') {
                            // MIND YOU we have a confirmaion window, right on he Method entrance, to confirm deletion:
                            // => return 'true' on window popup =>
                            // fake 'window.alert' to be a simple boolean - always returns true and never opens
                            window.confirm = () => true;
                            thisComponent.deleteTodo();
                            // Check it:
                            // expect(window.confirm).toHaveBeenCalledTimes(0);
                            // Would be fine, if Jasmine would let you spy on anything; but it doesn't: "Expected a spy, but got Function."
                            // So:
                            const spyOnWindowConfirm = spyOn(window, 'confirm').and.callFake(() => true);
                            const callSpyOnWindowConfirmMeth: (msg: string) => boolean = window.confirm;
                            expect(callSpyOnWindowConfirmMeth).toHaveBeenCalledTimes(0);
                        }
                        // ------------------------------------------------------------------------

                        // See what happened to the componentEmitter '@Output() manipulatedTodo':
                        expect(editedTodoToEmitBack).toBe(someEditedTodo);
                        // if (thisMethod !== 'deleteTodo') {
                        //     expect(editedTodoToEmitBack).toBe(someEditedTodo);
                        // } else {
                        //     // Needs the window.confirmation ok/cancel
                        //     expect(editedTodoToEmitBack).toBeUndefined();
                        // }
                        // And, for instance:
                        // Common props:
                        expect(editedTodoToEmitBack.action).toBe(thisMethod === 'deleteTodo' ? 'delete' : 'update');
                        expect(editedTodoToEmitBack.item.todo).toBe('TODO text');
                        expect(editedTodoToEmitBack.item.done).toBe(false);
                        // Changed props, on the Obj to emit back:
                        if (thisMethod === 'editTodoOnText') {
                            expect(editedTodoToEmitBack.propToUpdate).toEqual({ todo: thisComponent.todoText});
                        } else if (thisMethod === 'makeTodoDone') {
                            expect(editedTodoToEmitBack.propToUpdate).toEqual({ done: true});
                        } else {
                            expect(editedTodoToEmitBack.propToUpdate).toEqual(null);
                        }
                    }));
                    break;

                default:
                    break;
            }
        }

    });
});
