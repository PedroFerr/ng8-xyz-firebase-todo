import { Component, OnInit, Output, EventEmitter, ElementRef } from '@angular/core';
import { Item } from '../../ng-xyz-ui-lib.interfaces';

@Component({
    selector: 'xyz-todo-form-create',
    templateUrl: './todo-form-create.component.html',
    styleUrls: ['../_common/todo-form.component.scss']
})
export class TodoFormCreateComponent implements OnInit {

    isCreating = false;
    componentDOM: HTMLElement;
    todoText: string;

    @Output() newTodo = new EventEmitter<Item>();

    constructor(private hostElement: ElementRef) { }

    ngOnInit() {
        this.componentDOM = this.hostElement.nativeElement;
    }

    public userIsTyping(evt: KeyboardEvent) {
        const textArea = evt.target as any;
        this.todoText = textArea.value;
    }

    public createTodo() {
        const todoTextArea: HTMLElement = this.componentDOM.querySelector('textarea');

        if (this.todoText) {
            this.newTodo.emit({ todo: this.todoText, done: false, userId: undefined });

            todoTextArea.setAttribute('value', '');
            this.todoText = '';
            this.isCreating = false;
        }
    }

}
