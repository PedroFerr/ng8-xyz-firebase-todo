/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, EventEmitter } from '@angular/core';
// import { EventEmitter } from 'protractor';

import { TodoFormCreateComponent } from './todo-form-create.component';

import { Item } from '../../ng-xyz-ui-lib.interfaces';

describe('TodoFormCreateComponent', () => {
    let thisComponent: TodoFormCreateComponent;
    let fixture: ComponentFixture<TodoFormCreateComponent>;

    let textArea: HTMLElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TodoFormCreateComponent]
        })
        .compileComponents()
        .then(() => {        // => equivalent to 'beforeEach(() => {}),' OUTSIDE this 1st async beforeEach(() => {})
            fixture = TestBed.createComponent(TodoFormCreateComponent);
            thisComponent = fixture.componentInstance;
            fixture.detectChanges();
        });
    }));

    it('Should create TodoFormCreateComponent', () => {
        expect(thisComponent).toBeTruthy();
    });

    // Test 'x%' of the Component's METHODs, so test coverage will reach 'x%', on 'Functions()' column.
    describe('Should TodoFormCreateComponent METHODs do what\'s expected, on: ', () => {
        const methodsToTest: Array<string> = ['userIsTyping', 'createTodo'];

        for (const thisMethod of methodsToTest) {
            switch (thisMethod) {
                case 'userIsTyping':
                    it(`${thisMethod}() method: should equal 'this.todoText\' to the typed text` , async(() => {
                        expect(thisComponent.todoText).toBeUndefined();
                        // Trigger the method that reads waht user is typing
                        thisComponent.userIsTyping({target: { value: 'something'}} as unknown as KeyboardEvent);
                        // See if wht's typed is in 'this.todoText':
                        expect(thisComponent.todoText).toBe('something');
                    }));
                    break;

                case 'createTodo':
                    it(`${thisMethod}() method: should emit back TODO obj and clean everything`, async(() => {

                        // Don't forget that <textarea *ngIf="isCreating" />:
                        thisComponent.isCreating = true;
                        // DOM has changed:
                        fixture.detectChanges();
                        fixture.whenStable().then(() => {
                            const compiledEl: DebugElement = fixture.debugElement.query(By.css('textarea'));
                            textArea = compiledEl.nativeElement;

                            thisComponent.todoText = 'somethind different';    // for whatever means the application has built them.
                            // We have already 'something' on component's 'todoText'!
                            // => we can build our Todo OBJ to emit back:
                            const newTodoObj = {todo: thisComponent.todoText, done: false, userId: undefined };

                            let objToEmitBack: Item;
                            thisComponent.newTodo.subscribe((changes: EventEmitter<Item>) => objToEmitBack = newTodoObj);
                            // Check set up is correct:
                            expect(thisComponent.todoText).toBe('somethind different');
                            expect(objToEmitBack).toBeUndefined();

                            // OK. We can trigger the method that wants to emit 'newTodo'
                            // --------------------------
                            thisComponent.createTodo();
                            // --------------------------
                            // Changes were forced => 'objToEmitBack', that was undefined, should now have the 'newTodoObj'
                            // Check it:
                            expect(objToEmitBack).toBe(newTodoObj);
                            //
                            // And:
                            expect(thisComponent.todoText).toBe('');
                            expect(thisComponent.isCreating).toBe(false);
                        });
                    }));

                    // Now let's supposed we did NOT have anything on component's 'todoText'...
                    // This is obvious for the 'coverage' on 'Branches' - we are forcing the
                    // 'todo-form-create' @ "if (this.todoText) {} else { // nothing to do! }" code... Palhaçada!
                    it(`${thisMethod}() method: should have some text on \'this.todoText\', in order to emit back `, async(() => {
                        // Don't forget that <textarea *ngIf="isCreating" />:
                        thisComponent.isCreating = true;
                        // DOM has changed:
                        fixture.detectChanges();
                        fixture.whenStable().then(() => {
                            const compiledEl: DebugElement = fixture.debugElement.query(By.css('textarea'));
                            textArea = compiledEl.nativeElement;

                            thisComponent.todoText = null;    // for whatever means the application has built them.
                            // => we can build our Todo OBJ to emit back, even if no text is presented on the Todo
                            const newTodoObj = {todo: thisComponent.todoText, done: false, userId: undefined };

                            let objToEmitBack: Item;
                            thisComponent.newTodo.subscribe((changes: EventEmitter<Item>) =>
                                thisComponent.todoText ? objToEmitBack = newTodoObj : objToEmitBack = undefined
                            );
                            // Check set up is correct:
                            expect(thisComponent.todoText).toBeNull();
                            expect(objToEmitBack).toBeUndefined();

                            // OK. We can trigger the method that wants to emit 'newTodo'
                            // --------------------------
                            thisComponent.createTodo();
                            // --------------------------
                            // Changes were NOT forced => 'todoText' is NULL => 'objToEmitBack' is STILL udefined.
                            // But nothing should be emitted, since, by the App's code, !this.todoText:
                            // Check it:
                            expect(objToEmitBack).not.toBe(newTodoObj);
                            //
                            // And:
                            expect(thisComponent.todoText).not.toBe('');
                            expect(thisComponent.isCreating).not.toBe(false);
                        });

                    }));
                    break;

                default:
                    break;
            }
        }
    });
});
