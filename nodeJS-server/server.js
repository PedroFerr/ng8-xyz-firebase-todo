'use strict';

const serverPort = 3001;
// --- Dependencies
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
// --- Set up app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());


//--- GET root dir - for now, launch error to check it's working
app.get('/', (req, resp) => {
    // resp.send(`<h1>Hello World!</h1> <p>NodeJS here....</p>`);
    throw new Error('Watch the screen - a 500 simulation of an ERROR is printed. ;-)');
});


// --- Consider the uniformization of errors logging/launching:
app.use((err, req, resp, next) => {
    // log the error (some file filling...) and print error msg:
    console.log(err);
    resp.status(500).send('The thrown of a 500 ERROR is now simulated...');
})

//--- Launch the server on a Port
app.listen(serverPort, () => console.log('NodeJS + Express + nodemon SERVER just started on port ' + serverPort + '!'));