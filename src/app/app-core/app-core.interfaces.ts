export interface User {
    uid: string;
    email: string;
    displayName: string;
    photoURL: string;
    emailVerified: boolean;
    loginSource: string;
}

export type UserFormKeys = 'name' | 'email' | 'password';
export type FormValues = { [key in UserFormKeys]: string };

export interface FirebaseUser {
    uid: string;
    displayName: string;
    photoURL: string;
    email: string;
    emailVerified: boolean;
    phoneNumber: string;
    isAnonymous: boolean;
    tenantId: string;
    providerData: [];
    apiKey: string;
    appName: string;
    authDomain: string;
    stsTokenManager: {
        apiKey: string;
        refreshToken: string;
        accessToken: string;
        expirationTime: Date
    };
    redirectEventId: string;
    lastLoginAt: Date;
    createdAt: Date;
    multiFactor: {
        enrolledFactors: []
    };
}

export type FbAuthType = firebase.auth.UserCredential;
