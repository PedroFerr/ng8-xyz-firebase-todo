/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { HomepageComponent } from './homepage.component';

describe('HomepageComponent', () => {
    let thisComponent: HomepageComponent;
    let fixture: ComponentFixture<HomepageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HomepageComponent]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(HomepageComponent);
            thisComponent = fixture.componentInstance;
            fixture.detectChanges();
        });
    }));

    it('Should create HomepageComponent', () => {
        expect(thisComponent).toBeTruthy();
    });

    // Test 'x%' of the Component's METHODs, so test coverage will reach 'x%', on 'Functions()' column.
    describe('Should HomepageComponent METHODs do what\'s expected, on: ', () => {
        const methodsToTest: Array<string> = ['toggleMenu', 'onUserSignIn', 'onUserSignOut'];

        for (const thisMethod of methodsToTest) {
            const itString = thisMethod === 'onUserSignIn' ?
                `${thisMethod}() method: should set 2nd and 3rd <xyz-buttons-flex-bar /> buttons to ENABLE (istoDisableTopButtons(false))`
                :
                // onUserSignOut
                `${thisMethod}() method: should set 2nd and 3rd <xyz-buttons-flex-bar /> buttons to DISABLED (istoDisableTopButtons(true))`
            ;

            switch (thisMethod) {
                case 'toggleMenu':
                    it(`${thisMethod}() method: should toggle #wrapper DOM element class` , () => {
                        const wrapperDebugElem: DebugElement = fixture.debugElement.query(By.css('#wrapper'));
                        const wrapperElem: HTMLElement = wrapperDebugElem.nativeElement;
                        // Check initial conditions, before calling the Method:
                        expect(wrapperElem.classList).not.toContain('toggled');

                        // -----------------------------
                        thisComponent.toggleMenu();
                        // -----------------------------

                        expect(wrapperElem.classList).toContain('toggled');
                        // Toggle:
                        thisComponent.toggleMenu();
                        expect(wrapperElem.classList).not.toContain('toggled');
                    });
                    break;

                case 'onUserSignIn':
                case 'onUserSignOut':
                    it(itString, () => {
                        // -----------------------------
                        if (thisMethod === 'onUserSignIn') { thisComponent.onUserSignIn(); }
                        if (thisMethod === 'onUserSignOut') { thisComponent.onUserSignOut(); }
                        // -----------------------------
                        expect(thisComponent.topMenuDisabledButtons[2]).toBe(thisMethod === 'onUserSignIn' ? false : true);
                        expect(thisComponent.topMenuDisabledButtons[3]).toBe(thisMethod === 'onUserSignIn' ? false : true);
                    });
                    break;

                default:
                    break;
            }
        }

        // Get more 100& coverage ponts: test for a switch on all buttons... to true,
        // tslint:disable-next-line: max-line-length
        it(`istoDisableTopButtons() PRIVATE method should set the complete set of <xyz-buttons-flex-bar /> buttons to a unique boolean (true or false)` , () => {
            // disabling/enabling ALL of them:
            const booleans = [true, false];
            //
            // Check init conditions of the <xyz-buttons-flex-bar /> buttons.
            // MIND YOU:
            // 1) on the App's 'ngOnInit()', 2nd and 3rd buttons COULD be set to true/false, depending on local storage cookie...
            //     But as here, on Jasmine Unit Testing, there's no cookies => 2nd and 3rd button should be DISABLED => 'true';
            // 2) the last 5th button is DISABLED; since there's, for now, no code assigned to its clicking => alse strats on 'true'
            const btns = thisComponent.topMenuDisabledButtons;
            btns.forEach((btn: boolean, idx: number) => expect(btn).toBe(idx === 2 || idx === 3 || idx === 4 ? true : false));

            // We should be good to go now => trigger the method we want to test, for 'true' and for 'false'
            booleans.forEach( (setThisBoolean: boolean) => {
                // @ts-ignore => as it's a PRIVATE method (TS thing, NOT existent on JS) ignore TS, in order to compile the '.spec'
                thisComponent.istoDisableTopButtons(setThisBoolean);
                // Test expectations, refrehing the buttons array - should have different state, now:
                const newStateBtns = thisComponent.topMenuDisabledButtons;
                newStateBtns.forEach((eachBtn: boolean) => expect(eachBtn).toBe(setThisBoolean));
            });
        });
    });
});
