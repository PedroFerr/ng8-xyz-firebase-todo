/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { HomepageHomeComponent } from './homepage-home.component';

describe('HomepageHomeComponent', () => {
    let thisComponent: HomepageHomeComponent;
    let fixture: ComponentFixture<HomepageHomeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HomepageHomeComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HomepageHomeComponent);
        thisComponent = fixture.componentInstance;
        // Tell the TestBed to perform data binding, first.
        // Well... just a "standard" boilerplate; there's, till now, nothing to bind, here.
        // Would be compulsory if you had, i.e., Services Providers declaration - your component had some kind of 'constructor()' injections
        // But here, on this specifig Unit test, you do can comment next line - tests will still result green:
        fixture.detectChanges();
    });

    it('Should create HomepageHomeComponent', () => {
        expect(thisComponent).toBeTruthy();
    });

    it(`HomepageHomeComponent should have as title 'Welcome to XYZ Reality!'`, () => {
        expect(thisComponent.pageTitle).toEqual('Welcome to XYZ Reality!');
    });

    it('HomepageHomeComponent should render title in an <h1 /> tag', () => {
        const compiled = fixture.debugElement.nativeElement;
        const h1 = compiled.querySelector('h1');
        // Be sure DOM is ready and settled down, first:
        fixture.detectChanges();

        expect(h1.textContent).toContain(thisComponent.pageTitle);
    });

});

// About 'fixture.detectChanges();', if you have 'sync' (in the sense of 'at the same time', 'only once', 'at the beginning')
// DOM interaction, you can set it automatically:
//
// TestBed.configureTestingModule({
//     declarations: [ ... ],
//     ...
//     providers: [
//       { provide: ComponentFixtureAutoDetect, useValue: true }
//     ]
//     //
//     // You don't need to enforce 'fixture.detectChanges();' anymore, anywere on your '*.spec.ts' you're testing
// });
//
// MIND YOU:
// The ComponentFixtureAutoDetect service responds to asynchronous activities (promise resolution, timers, and DOM events).
// But a direct, synchronous update of the component property is invisible and might be needed.
// If so, the test MUST call fixture.detectChanges() manually, here and there,
// to trigger another cycle of change detection - when DOM gets changed, i.e.:
//
// it('should convert foo name to Title Case', () => {
//     const hostElement = fixture.nativeElement;
//     const nameInput: HTMLInputElement = hostElement.querySelector('input');
//     const nameDisplay: HTMLElement = hostElement.querySelector('span');

//     nameInput.value = 'foo';
//     nameInput.dispatchEvent(newEvent('input'));
//     fixture.detectChanges();
//     expect(nameDisplay.textContent).toBe('FOO');
// }
