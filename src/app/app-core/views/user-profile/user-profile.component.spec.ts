/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { UserProfileComponent } from './user-profile.component';

import { AngularFireAuth } from '@angular/fire/auth';

class MockAngularFireAuthService implements Partial<AngularFireAuth> {
    // -----------------------------------------------------
    // Since it's a parial implementation, we don't need to code any Method.
    // See MockAuthService block comment @ 'homepage-login.component.spec.ts', for better understanding.
    // ....
    // -----------------------------------------------------
}

describe('UserProfileComponent', () => {
    let thisComponent: UserProfileComponent;
    let fixture: ComponentFixture<UserProfileComponent>;
    let afAuth: AngularFireAuth;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [UserProfileComponent],
            providers: [ AngularFireAuth ]
        }).compileComponents();

        fixture = TestBed.overrideComponent( UserProfileComponent, { set:
            {
                providers: [
                    { provide: AngularFireAuth, useClass: MockAngularFireAuthService }
                ]
            }
        }).createComponent(UserProfileComponent);

        // Finally:
        thisComponent = fixture.componentInstance;
        // and:
        afAuth = fixture.debugElement.injector.get(AngularFireAuth);     // <= should return, now, MockAngularFireAuthService
        // Ensure that the mock data is available:
        fixture.detectChanges();
    }));

    it('Injected AngularFireAuth on the Component should be an instance of MockAngularFireAuthService', () => {
        expect(afAuth instanceof MockAngularFireAuthService).toBeTruthy();
    });

    it('Should create UserProfileComponent, having AngularFireAuth INJECTED Service mocked up', () => {
        expect(thisComponent).toBeTruthy();
    });
});
