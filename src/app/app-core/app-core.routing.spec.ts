import { TestBed, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { Location } from '@angular/common';
import { Router, RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot, NavigationExtras } from '@angular/router';

import { routes } from './app-core.routing';

import { HomepageHomeComponent } from './views/home/contents/homepage-home/homepage-home.component';
import { UserProfileComponent } from './views/user-profile/user-profile.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';

import { AuthService } from './auth/firebase-auth.services';
// That will be called by the Router Guard:
import { AuthGuard } from './auth/auth-guard.service';

// Our fake Router should be as close as possible to the Angular Router on the component's Routing list
class MockRouter implements Partial<Router> {
    navigate(commands: any[], extras?: NavigationExtras): Promise<boolean> {
        return {} as Promise<true>;
    }
}

// class MockAuthService {
//
// Better: we pretend that this fake class is as close as possible to AuthService
// but with fewer properties - we only want to know if User is logged in - and eventual if email is validated
class MockAuthService implements Partial<AuthService> {

    userIsLoggedIn: boolean;
    userHasValidatedEmail: boolean;

    constructor(
        isUserLoggedIn: boolean,
        isUserEmailValidated: boolean
    ) {
        this.userIsLoggedIn = isUserLoggedIn;
        this.userHasValidatedEmail = isUserEmailValidated;
    }

    get isLoggedIn(): boolean { return this.userIsLoggedIn; }
    get isMailValidated(): boolean { return this.userHasValidatedEmail; }

    isItLoggedIn(): boolean { return this.isLoggedIn; }
    isLoggedInEmailValidated(): boolean { return this.isMailValidated; }
}


describe('AppCoreRoutingModule', () => {
    let location: Location;
    let router: Router;
    let fixture: ComponentFixture<HomepageHomeComponent>;

    let fakeRouter: MockRouter;

    // let fakeAuthService: MockAuthService;  // <= NOPES! Will not allow a "free" 'authGuard = new AuthGuard(fakeAuthService, router);'
    let fakeAuthService: any;

    // This must be the real one - we want to check REAL 'authGuard.canActivate()' returning boolean
    let authGuard: AuthGuard;
    let authGuardAnswer: boolean;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule.withRoutes(routes)],
            declarations: [HomepageHomeComponent, UserProfileComponent, PageNotFoundComponent],
        });

        fixture = TestBed.createComponent(HomepageHomeComponent);

        // ----------------------------------------------------------

        router = TestBed.inject(Router);
        fakeRouter = new MockRouter();

        location = TestBed.inject(Location);

        router.initialNavigation();
    });

    describe(`AuthGuard 'canActivate()', for the protected routes,`, () => {
        describe(`should return TRUE for a LOGGED IN User,`, () => {
            beforeEach(() => {
                // Have to fake that the AuthService, which enquires Firebase asking:
                // "Hey you! Is this guy authored...?"
                // for Firebase to respond:
                // "Yes! Trust me! The "this.authService.isLoggedIn" Method is definetely, and fakely, returning TRUE, sir!"
                //
                // fakeAuthService = { get isLoggedIn(): boolean { return true; } };
                //
                fakeAuthService = new MockAuthService(true, true);
                authGuard = new AuthGuard(fakeAuthService, router);
                // So Angular's Router AuthGuard alse checks to TRUE
                authGuardAnswer = authGuard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot);
            });

            it(`allowing '/profile' route to be called`, fakeAsync(() => {
                // router.navigate(['/profile']).then(() => {
                //     expect(location.path()).toBe('/profile');
                // });
                // Upper will go for the Angular component and, in fact, no AuthGuard was defined HERE, on this spec file code.
                // We did have defined a FAKE one - but Jasmine ain't fool! :-)
                // Obviously Jasmine throws the error: "NullInjectorError: No provider for AuthGuard!"
                //
                // So we 'spyOn([a fake router])', that has a REAL 'authGuard()' (that's what we're testing, right?)
                // questioning a FAKE Service and expect() what's possible on a 'fake' world - never on the real, component's one!
                const spyOnFakeNavigation = spyOn(fakeRouter, 'navigate');
                // And call it:
                const callTheSpy = fakeRouter.navigate(['/profile']);        // <= MIND YOU this is a 'fakeAsync()' it()
                if (authGuardAnswer === false) {
                    // Router shall re-direct User to the root Route:
                    fakeRouter.navigate(['/']);
                }
                // And see what's expected, regarding the 'canActivate()' Guard, that should be guarding this Route:
                expect(authGuardAnswer).toEqual(true);

                // And, if upper is in fact true, the User was NOT redirected to the (fake! ;-)) root Route but to the '/profile' one:
                expect(spyOnFakeNavigation).toHaveBeenCalledWith(['/profile']);
                expect(spyOnFakeNavigation).not.toHaveBeenCalledWith(['/']);
            }));

            it(`allowing '/todos' route to be called`, fakeAsync(() => {
                const spyOnFakeNavigation = spyOn(fakeRouter, 'navigate');
                const callTheSpy = fakeRouter.navigate(['/todos']);            // <= MIND YOU this is a 'fakeAsync()' it()
                if (authGuardAnswer === false) {
                    // Router shall re-direct User to the root Route:
                    fakeRouter.navigate(['/']);
                }
                expect(spyOnFakeNavigation).not.toHaveBeenCalledWith(['/']);
                expect(spyOnFakeNavigation).toHaveBeenCalledWith(['/todos']);
                // And, just for fun:
                expect(spyOnFakeNavigation).not.toHaveBeenCalledTimes(2);
                expect(spyOnFakeNavigation).toHaveBeenCalledTimes(1);
            }));
        });


        describe(`should return FALSE for a NON LOGGED User,`, () => {
            beforeEach(() => {
                // Now we want the Service to return FALSE to the AuthGuard enquire to it:
                //
                // fakeAuthService = { get isLoggedIn(): boolean { return false; } };
                //
                fakeAuthService = new MockAuthService(false, false);
                authGuard = new AuthGuard(fakeAuthService, router);
                authGuardAnswer = authGuard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot);
            });

            it(`REDIRECTING '/profile' and '/todos' GUARDED routes to '/'`, fakeAsync(() => {
                // 1st: AuthGuard says it's FALSE - no one was logged in, regarding the question answered by the FAKE AuthService:
                expect(authGuardAnswer).toEqual(false);

                // And then, regarding Angular Routing list:
                const spyOnFakeNavigation = spyOn(fakeRouter, 'navigate');
                fakeRouter.navigate(['/profile']);                            // <= MIND YOU this is a 'fakeAsync()' it()
                if (authGuardAnswer === false) {
                    fakeRouter.navigate(['/']);
                }
                expect(spyOnFakeNavigation).toHaveBeenCalledWith(['/']);

                fakeRouter.navigate(['/todos']);
                if (authGuardAnswer === false) {
                    fakeRouter.navigate(['/']);
                }
                expect(spyOnFakeNavigation).toHaveBeenCalledWith(['/']);

                // By the way:
                expect(spyOnFakeNavigation).not.toHaveBeenCalledTimes(1);
                expect(spyOnFakeNavigation).not.toHaveBeenCalledTimes(2);
                expect(spyOnFakeNavigation).toHaveBeenCalledTimes(4);
            }));
        });
    });

});
