import { RouterReducerState } from '@ngrx/router-store';

import { FirebaseUser as OurFirebaseUser } from 'src/app/app-core/app-core.interfaces';
import { HttpErrorResponse } from '@angular/common/http';

export interface IMainAppModulesState {
    router?: RouterReducerState;
    authenticationOfAnUser: IAuthenticatedUser;
}

export const initialMainAppModulesState: IMainAppModulesState = {
    router: {} as RouterReducerState,
    authenticationOfAnUser: null
};

export interface IAuthenticatedUser {
    isAPIbeingCalled: boolean;

    errorMessage: HttpErrorResponse | null;
    authData: OurFirebaseUser | null;
}
