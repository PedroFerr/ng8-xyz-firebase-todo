import { TestBed, fakeAsync } from '@angular/core/testing';
import { AngularFirestore } from '@angular/fire/firestore';

import { Observable, of, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

import { FirebaseTodosService } from './firebase-todos.services';
import { Item, ItemWithId, DocumentChangeAction } from '../todos.interfaces';
import { not } from '@angular/compiler/src/output/output_ast';

describe(`FirebaseTodosService, OUR suite of Methods to call Google's Firebase Service, to CRUD our TODOs,`, () => {

    let service: FirebaseTodosService;

    const MockTODO = {
        title: 'Example Post',
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla commodo dui quis.'
    };

    const catchOperationErrorSpy = jasmine.createSpyObj({
        // tslint:disable-next-line: max-line-length
        // catch: () => null   // could have fake it a little bit more real ;-) 'catch: (error: {message: 'Error faked'}) => console.error(error)'
        // catch: throwError({status: 404})
        catch: () => throwError({status: 404})
    });


    // const mapToDocsWithId = () =>
    //     (docChanges: any) => docChanges.map(
    //         (docAction: DocumentChangeAction) => MockTODO
    //     )
    // ;
    const mapToDocsWithIdSpy = jasmine.createSpyObj({
        map: of([MockTODO])
        // map: (docChanges: any) => docChanges.map(
        //     (docAction: DocumentChangeAction) => MockTODO
        // )
    });

    const pipeOnsnapshotChangesSpy = jasmine.createSpyObj({
        // pipe: of([MockTODO]).pipe(
        //     map(
        //         (docChanges: any) => docChanges.map(
        //             (docAction: DocumentChangeAction) => MockTODO
        //         )
        //     )
        // )
        //
        // pipe: map(
        //     (docChanges: any) => docChanges.map(
        //         (docAction: DocumentChangeAction) => MockTODO
        //     )
        // )
        //
        pipe: mapToDocsWithIdSpy
        // pipe: mapToDocsWithId
        // )
    });

    // ---------------------------------------------------
    // Our AngularFirestore "faked non-SQL db" with 2 props:
    // let FirebaseDB: any;
    // ---------------------------------------------------
    // 1) the collection (of 'todos')
    const collectionSpy = jasmine.createSpyObj({
        // snapshotChanges: of([MockTODO]),
        snapshotChanges: pipeOnsnapshotChangesSpy,

        // add: of(MockTODO),
        //
        // Nopes...On 'firabese-todos.servic.ts', after '.add' (on 'createTodo()') we chain a '.catch' error:
        add: catchOperationErrorSpy
        // add: Promise.resolve(throwError({status: 404})).then(() => catchOperationErrorSpy) // catchOperationErrorSpy
        // add: () => throwError({status: 404})
        // add: () => {
        //     throwError({status: 404});
        //     return catchOperationErrorSpy();
        // }

    });
    // 2) and the document(s), which contain each 'todo'
    const collectionDocumentSpy = jasmine.createSpyObj({
        update: catchOperationErrorSpy,
        delete: catchOperationErrorSpy
    });
    // ---------------------------------------------------

    const FirebaseDB = jasmine.createSpyObj('AngularFirestore', {
        collection: collectionSpy,
        doc: collectionDocumentSpy
    });


    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                FirebaseTodosService,
                { provide: AngularFirestore, useValue: FirebaseDB }
            ]
        });

        service = TestBed.inject(FirebaseTodosService);
    });

    it(`should have a getter, over AngularFirestore DB, for the 'todos' (collection of documents) list`, () => {
        service.getTodos();
        // Check all spyes, here, were called by triggering the Method there, on our FirebaseTodosService Class:
        expect(collectionSpy.snapshotChanges).toHaveBeenCalled();
        expect(pipeOnsnapshotChangesSpy.pipe).toHaveBeenCalled();
        expect(FirebaseDB.collection).toHaveBeenCalledWith('todos');
        //
        // expect(collectionSpy.snapshotChanges).toEqual(of(MockTODO));
        // expect(mapToDocsWithIdSpy.map).toEqual(of(MockTODO));
    });

    // Now, go for the 100% coverage,
    // by refactoring 'const', in 'firabese-todos.servic.ts' into getters (spyes will call them!)
    it(`should CRUD the 'todos' collection documents`, () => {
        service.createTodo({} as Item);
        expect(collectionSpy.add).toHaveBeenCalled();

        const manipulatedTodo = {} as ItemWithId;

        service.updateTodo(manipulatedTodo, {done: false});
        expect(collectionDocumentSpy.update).toHaveBeenCalled();

        service.deleteTodo(manipulatedTodo);
        expect(collectionDocumentSpy.update).toHaveBeenCalled();
    });

    it(`should console.log if throwError any AngularFirestore CRUD operation ERROR`, fakeAsync(() => {
        // const fakeError = {} as Promise<((error: Error) => void)>;
        // const fakeError = throwError({status: 404} as Promise<((error: Error) => void)>);
        const fakeError = new Promise<((error: Error) => void)>(
            (resolve, reject) => {
                // throwError({status: 404});
                // resolve();
                //
                // reject();
                // reject(() => null );
                //
                // --------------------------------
                // reject(() => throwError({status: 404}) );
                // --------------------------------
                // UPPER is the best I could reach!
                // Though it throws Jasmine breaks, you can read on the console the handleError()  msgs
                // Comment it, so tests succeded...
            }
        );
        const spyOnAngularFirestoreError = spyOn(service, 'createTodo')
            // --------------------------------
            .and.callFake(() => fakeError)
            // --------------------------------
            // UPPER is the best I could reach!
            // Though Jasmine breaks, you can read on the console:
            //
            // ERROR: '=============================='
            // Chrome 83.0.4103 (Windows 8.1.0.0): Executed 37 of 52 (1 FAILED) (0 secs / 0.645 secs)
            // WARN: 'A Firebase Service ERROR occurred: ', 'Error faked'
            // Chrome 83.0.4103 (Windows 8.1.0.0): Executed 37 of 52 (1 FAILED) (0 secs / 0.645 secs)
            // ERROR: '=============================='
            //
            // Meaning our 'handleError()' has, in fact, been called!
            //
            // But why does Jasmine breaks....?!?!?
            // "Error: Uncaught (in promise): () => Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])({ status: 404 })"
        ;
        service.createTodo({} as Item);
        expect(spyOnAngularFirestoreError).toHaveBeenCalled();
        //
        // expect(spyOnAngularFirestoreError).toEqual(fakeError);
        // expect(catchOperationErrorSpy.catch).toHaveBeenCalledTimes(3);
    }));

    it(`that should, operation ERROR, be caught by an 'handleError' `, () => {
        // @ts-ignore // <= PRIVATE method => it'a a TS thing, non existent on JavaScript, that will, in fact, run this Unit Test
        const spyOnHandleError = spyOn(service, 'handleError').and.callThrough();
        // @ts-ignore
        service.handleError({message: 'Error faked'});
        expect(spyOnHandleError).toHaveBeenCalled();
    });

});
