import { Injectable } from '@angular/core';

import {
    AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument,
    DocumentData, DocumentReference
} from '@angular/fire/firestore';

import { Observable, of, BehaviorSubject, combineLatest, PartialObserver, OperatorFunction } from 'rxjs';
import { switchMap, map, withLatestFrom, tap, finalize, catchError } from 'rxjs/operators';

import { Item, DocumentChangeAction, ItemWithId } from '../todos.interfaces';
import { FirebaseUser } from 'src/app/app-core/app-core.interfaces';

@Injectable({
    providedIn: 'root'
})
export class FirebaseTodosService {

    // Our Auth User Array of TODOs:
    userAuthdocs: Array<ItemWithId> = [];
    // Our Auth User, and filtered, Array of TODOs:
    authDoneFilteringTodos$: Observable<Array<ItemWithId>>;
    authTodos$: BehaviorSubject<string | null>;
    doneTodos$: BehaviorSubject<boolean | null>;

    /**
     * Nice OFFICIAL reading for Firebase DATA manipulation:
     * https://github.com/angular/angularfire2/blob/7eb3e51022c7381dfc94ffb9e12555065f060639/docs/firestore/collections.md
     *
     * @param db - our No-SQL DB at Google's Cloud Firebase
     * https://console.firebase.google.com/project/xyz-reality-todos/database/firestore
     *
     */
    constructor(private db: AngularFirestore) { }

    /**
     * Getter for our Google's Firebase collection of 'todos'
     *
     * @param collectionName - we have set it on Firebase console to be 'todos'
     */
    getFirestoreDB(collectionName: string = 'todos'): AngularFirestoreCollection {
        return this.db.collection<Item>(collectionName);
    }

    getFirebaseDBdocState(): Observable<Array<DocumentChangeAction>> {
        return this.getFirestoreDB().snapshotChanges();
    }

    // -------------------------------------------------------------------------------------
    mapToDocsWithId = (): any => map( this.getActionsByChanges );
    //
    // mapToDocsWithId(): OperatorFunction<any, any> {
    // NOPES!
    // gets worst, coverage...
    // Watch the HUGE diference between 'mapToDocsWithId = () =>' and 'mapToDocsWithId() {}'
    // ====================================
    // =======  ALWAYS use "() =>"  =======
    // ====================================
    //
    //     return map( this.getActionsByChanges );
    //     // return map(
    //     //     (docChanges: any) => docChanges.map(
    //     //         (docAction: DocumentChangeAction) => {
    //     //             const data = docAction.payload.doc.data() as Item;
    //     //             const autoID: string = docAction.payload.doc.id;
    //     //             // And so:
    //     //             const doc = { autoID, ...data };

    //     //             // // Now... we (better; the user!) can have a:
    //     //             // const filteredDoc: ItemWithId = filter ? this.filteredDoc(filter, doc) : doc;
    //     //             // // But WE shon't forget, lastly the:
    //     //             // const docToReturn: ItemWithId = ofThisUser ? this.userAuthDoc(ofThisUser, filteredDoc) : doc;

    //     //             // return docToReturn;
    //     //             return doc;
    //     //         }
    //     //     )
    //     // );
    // }

    // getActionsByChanges = (): any => map( this.getDocByActions );
    // NOPES!
    // You gain a tiny bit of coverage BUT, obviously, NO 'todos' will be fetched!
    // We do need 'docChanges' coming from upper 'mapToDocsWithId()'...
    //
    // getActionsByChanges(docChanges: any): any { return docChanges.map( this.getDocByActions ); }
    // "() =>" is better, for coverage:
    getActionsByChanges = (docChanges: any) => docChanges.map( this.getDocByActions );
    //
    // Better: call getDocByActions() code directly:
    // getActionsByChanges = (docChanges: any) => docChanges.map( (docAction: DocumentChangeAction) => {
    //
    // getDocByActions(docAction: DocumentChangeAction): ItemWithId {
    getDocByActions = (docAction: DocumentChangeAction): ItemWithId => {
            const data = docAction.payload.doc.data() as Item;
            const autoID: string = docAction.payload.doc.id;
            // And so:
            const doc: ItemWithId = { autoID, ...data };

            // // Now... we (better; the user!) can have a:
            // const filteredDoc: ItemWithId = filter ? this.filteredDoc(filter, doc) : doc;
            // // But WE shon't forget, lastly the:
            // const docToReturn: ItemWithId = ofThisUser ? this.userAuthDoc(ofThisUser, filteredDoc) : doc;

            // return docToReturn;
            return doc;
    }
    // })

    getFirebaseDBallDocsWithId(): Observable<Array<ItemWithId>>  { return this.getFirebaseDBdocState().pipe(this.mapToDocsWithId()); }
    // "() =>" is better, for coverage:
    // tslint:disable-next-line: max-line-length
    // getFirebaseDBallDocsWithId = (): Observable<Array<ItemWithId>> => this.getFirebaseDBdocState().pipe( map( this.mapToDocsWithId ));
    // We can shortcut, saving 1 function.
    // NOPES!
    // It's WORST...
    // ====================================
    // =======  NEVER shortcut !! =========
    // use ALWAYS as MANY "() =>" as possible.
    // ====================================
    // Though seems not, it will compensate at the end,
    // when we're talking about Unit Test % coverage !
    // ====================================
    // getFirebaseDBallDocsWithId = (): Observable<Array<ItemWithId>> => this.getFirebaseDBdocState().pipe( map(this.getActionsByChanges));

    // tslint:disable-next-line: max-line-length
    // getFirebaseDBallDocsWithId(): Observable<Array<ItemWithId>> { return this.getFirebaseDBdocState().pipe(map( this.getActionsByChanges )); }
    // return this.getFirebaseDBdocState().pipe(this.mapToDocsWithId());
        //
        // return this.getFirebaseDBdocState().pipe(
        //     map( this.getActionsByChanges )
        //     // map( (docChanges: any) => docChanges.map( this.getDocByActions ) )   // AGAIN, NO 'todos' will be fetched!
        //     // map( map( this.getDocByActions ) )                                   // <= would be GREAT, if I could do this...
        // );
    // }
    // -------------------------------------------------------------------------------------

    /**
     * Getter to get ONE 'todo' from our Google's Firebase collection of 'todos'
     * @param manipulatedTodo - the 'todo' that was changed by the User through our Application
     * @param collectionName - the Google's Firebase collection where this 'todo' is
     */
    getFirestoreDBdoc(manipulatedTodo: ItemWithId, collectionName: string = 'todos'): AngularFirestoreDocument<ItemWithId> {
        return this.db.doc(`${collectionName}/${manipulatedTodo.autoID}`);
    }

    /**
     * The Google's Cloud Firebase is a No-SQL Database, so a non related collection of Objects (what many people call WRONGLY a JSON! ;-)
     * Having said that, each DB Item (a TODO) is considered a document.
     *
     * If you need the doc.id() in the TODO Item, so it can be updated/deleted, you MUST query with '.snapshotChanges()'
     * '.snapshotChanges()' will then return an array of 'DocumentChangeAction's,
     * which contains a lot of information about "what happened" with each change on this 'document' / 'DB Item'
     *
     * Each 'DocumentChangeAction' is, so, an Object with 2 props:
     *  - type: what DocumentChangeType operation occured (added, modified, removed)
     *  - payload: important metadata about the change and a doc property which is the DocumentSnapshot.
     *
     * So, let's query our DB!
     *
     * KEEP IN MIND
     * -----------------------------
     * GetTodos PER LOGGED IN User
     * (filtered through doc.userId)
     * -----------------------------
     * =========================================
     *
     * REPLACED BY next 'getAuthTodos()' and 'getFilteredAuthTodos()'
     * NOPES!
     * Couldn't make it... we would need to query the DB, already with the wanted filter,
     * and NOT trying to filter the whole collection here, on the 'fetch'.
     *
     * So...
     * We'll query through the HTML, doing a simple array filter, by Obj props, here, AFTER the Firebase query returning ALL 'todos'
     *
     * @param filter - optional. If any ItemWithId prop ('done' true/false) is to be filtered from
     * @param ofThisUser - optional. The current logged in User
     */
    // tslint:disable-next-line: max-line-length
    getTodos = (ofThisUser?: FirebaseUser, filter?: 'todos' | 'dones' | null): Observable<Array<ItemWithId>> => this.getFirebaseDBallDocsWithId();
        //
        // const docs$ = this.getFirebaseDBdocState().pipe(this.mapToDocsWithId());
        // const docs$ = this.getFirebaseDBdocState().pipe(
        //     map(
        //         (docChanges: any) => docChanges.map(
        //             (docAction: DocumentChangeAction) => {
        //                 const data = docAction.payload.doc.data() as Item;
        //                 const autoID: string = docAction.payload.doc.id;
        //                 // And so:
        //                 const doc = { autoID, ...data };

        //                 // // Now... we (better; the user!) can have a:
        //                 // const filteredDoc: ItemWithId = filter ? this.filteredDoc(filter, doc) : doc;
        //                 // // But WE shon't forget, lastly the:
        //                 // const docToReturn: ItemWithId = ofThisUser ? this.userAuthDoc(ofThisUser, filteredDoc) : doc;

        //                 // return docToReturn;
        //                 return doc;
        //             }
        //         )
        //     )
        //     // , tap( (docs) => {
        //     //     console.warn('Doc of', ofThisUser, docChanges);
        //     //     this.userAuthdocs.push(docs);
        //     // })
        //     , catchError((error: Error) => of(error))
        // );

        // // Check what, and when, we are (scalar) returning back:
        // console.warn('Docs of', ofThisUser, this.userAuthdocs);
        // // Remove the falsys (empty, null, undefined, etc) docs, result of filtering or authoring:
        // const validUserAuthDocs = this.userAuthdocs.filter(n => n);
        // console.warn('VALID Docs of', ofThisUser, validUserAuthDocs);

        // return docs$;
    // }

    // getAuthTodos() {

    // }

    /**
     * =========================================
     *     NEXT IS NOT BEING USED ANYMORE!
     * =========================================
     *
     * @param ofThisUser - the current logged in User
     * @param filter - if any ItemWithId prop ('done' true/false) is to be filtered from
     */
    // getFilteredAuthTodos(ofThisUser: FirebaseUser, filter?: 'todos' | 'dones' | null) {
    //     const fbUserId = ofThisUser.uid;
    //     const doneBool = filter === 'todos' ? false : true;

    //     this.authTodos$ = new BehaviorSubject(null);
    //     this.doneTodos$ = new BehaviorSubject(null);

    //     this.authDoneFilteringTodos$ = combineLatest(
    //         [this.authTodos$, this.doneTodos$]
    //     ).pipe(
    //         switchMap(([userId, done]: [string, boolean]) =>
    //             this.db.collection('todos', ref => {
    //                 let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
    //                 if (userId) { query = query.where('userId', '==', fbUserId); }
    //                 if (done) { query = query.where('done', '==', doneBool); }
    //                 return query;
    //             }).valueChanges()
    //         )
    //     ) as any;
    // }

    createTodo = (newTodo: Item): Promise<DocumentReference | ((error: Error) => void)> =>
        this.getFirestoreDB()
            .add(newTodo)
            .catch((error: Error) => this.handleError)
            // .catch(err => this.handleError)
    // }

    updateTodo = (manipulatedTodo: ItemWithId, onProp: { [key: string]: string| boolean}): Promise<void | ((error: Error) => void)> =>
        // const DBdoc: AngularFirestoreDocument<ItemWithId> = this.db.doc(`todos/${manipulatedTodo.autoID}`);
        this.getFirestoreDBdoc(manipulatedTodo)
            .update(onProp)
            .catch(err => this.handleError)

        // If something seems wrong, next is better for TESTING: Check it @ https://firebase.google.com/docs/firestore/manage-data/add-data
        // If the item does not exists, it just gets created with this single prop  - you can then inspect at the Firebase Console!
        // DBdoc.set(onProp, { merge: true }).catch((error: Error) => this.handleError);
    // }

    deleteTodo = (manipulatedTodo: ItemWithId): Promise<void | ((error: Error) => void)> =>
        // const DBdoc: AngularFirestoreDocument<ItemWithId> = this.db.doc(`todos/${manipulatedTodo.autoID}`);
        this.getFirestoreDBdoc(manipulatedTodo)
            .delete()
            .catch(err => this.handleError)
    // }

    /**
     * =========================================
     *     NEXT IS NOT BEING USED ANYMORE!
     * =========================================
     *
     * User has the feature of filtering the TODOs listing,
     * on each of the boolean state they can have: docs 'todo' or 'done'
     *
     * @param thisFilter - the wanted filtering, coming fom the Web Surfer user UX
     * @param thisDoc - the current Doc we are analysing, on Firebase DB to send it to the list of TODODs or not.
     */
    // private filteredDoc(thisFilter: 'todos' | 'dones' | null, thisDoc: ItemWithId): ItemWithId {
    //     const emptyDoc = {} as ItemWithId;

    //     if (thisFilter === 'todos') {
    //         return thisDoc.done === true ? emptyDoc : thisDoc;
    //     } else if (thisFilter === 'dones') {
    //         return thisDoc.done === true ? thisDoc : emptyDoc;
    //     }

    // }

    /**
     * =========================================
     *     NEXT IS NOT BEING USED ANYMORE!
     * =========================================
     *
     * It's a business rule: each user can only see, and manipulate, their own TODOs list
     *
     * @param belongingToThisUser - our logged in Firebase User
     * @param thisDoc - the current Doc we are analysing, on Firebase DB to send it to the list of TODODs or not.
     */
    // private userAuthDoc(belongingToThisUser: FirebaseUser, thisDoc: ItemWithId): ItemWithId {
    //     const emptyDoc = {} as ItemWithId;
    //     return thisDoc.userId === belongingToThisUser.uid ? thisDoc : emptyDoc;
    // }

    private handleError = (error: Error): void => {
        console.error('==============================');
        console.warn('A Firebase Service ERROR occurred: ', error.message);
        console.error('==============================');
    }

}
