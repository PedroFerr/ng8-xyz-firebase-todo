/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { Observable, of } from 'rxjs';

// Redux Store:
import { StoreModule, Store, MemoizedSelector, DefaultProjectorFn } from '@ngrx/store';
import { IMainAppModulesState, IAuthenticatedUser } from 'src/app/app-core/redux-store/store-pack.state.interfaces';
import { authenticationOfAnUser } from 'src/app/app-core/redux-store/store-pack.selectors';

// 3 Services called on the component's constructor()
import { FirebaseTodosService } from './services/firebase-todos.services';
import { AuthService } from 'src/app/app-core/auth/firebase-auth.services';
import { AngularFireAuth } from '@angular/fire/auth';

import { TodosComponent } from './todos.component';

// Interfaces:
import { Item, ItemWithId } from './todos.interfaces';
import { FirebaseUser } from 'src/app/app-core/app-core.interfaces';
import { DocumentReference } from 'angularfire2/firestore';



class MockAuthService implements Partial<AuthService> { }
class MockAngularFireAuthService implements Partial<AngularFireAuth> {

}
class MockFirebaseTodosService implements Partial<FirebaseTodosService> {
    getTodos(ofThisUser?: FirebaseUser, filter?: 'todos' | 'dones' | null): Observable<Array<ItemWithId>> {
        return of([] as Array<ItemWithId>);
    }
    createTodo = (): Promise<DocumentReference> => new Promise<DocumentReference>((resolve, reject) => resolve());
    // tslint:disable-next-line: max-line-length
    updateTodo = (thisTodoItem: ItemWithId,  onProp: { [key: string]: string| boolean}): Promise<void> => new Promise<void>((resolve, reject) => resolve());
    deleteTodo = (thisTodoItem: ItemWithId): Promise<void> => new Promise<void>((resolve, reject) => resolve());
}

class MockReduxStore implements Partial<Store<IMainAppModulesState>> {
    // And the Redux property select() to fetch values of a selector Observable in the sliced Store:
    select(
        storeCreateSelector: MemoizedSelector<object, IAuthenticatedUser, DefaultProjectorFn<IAuthenticatedUser>>
    ): Observable<IAuthenticatedUser> {
        return of({} as IAuthenticatedUser);
    }
}

describe('TodosComponent', () => {
    let thisComponent: TodosComponent;
    let fixture: ComponentFixture<TodosComponent>;

    let authService: AuthService;
    let afAuth: AngularFireAuth;

    let fbDBService: FirebaseTodosService;
    let useLessMockFirebaseTodosService: MockFirebaseTodosService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ StoreModule.forRoot({}) ],
            declarations: [TodosComponent],
            providers: [
                AuthService, AngularFireAuth
                // And in case you need them, for some useless fake test, with these useless mocked Services:
                , MockFirebaseTodosService
            ]
        })
        .compileComponents()
        .then(() => {
            thisComponent = fixture.componentInstance;
            authService = fixture.debugElement.injector.get(AuthService);    // <= should return, after 'overrideComponent', MockAuthService
            afAuth = fixture.debugElement.injector.get(AngularFireAuth);     // <= should return, now, MockAngularFireAuthService
            fbDBService = fixture.debugElement.injector.get(FirebaseTodosService);   // <= should return, now, MockFirebaseTodosService

            fixture.detectChanges();
        });

        fixture = TestBed.overrideComponent( TodosComponent, { set:
            {
                providers: [
                    { provide: AuthService, useClass: MockAuthService },
                    { provide: AngularFireAuth, useClass: MockAngularFireAuthService },
                    { provide: FirebaseTodosService, useClass: MockFirebaseTodosService },
                    { provide: Store, useClass: MockReduxStore }
                ]
            }
        }).createComponent(TodosComponent);

        // Before leaving:
        // Rather usefull, though pretty useless ;-), is testing the currently MOCKED service methods/properties, when in need:
        useLessMockFirebaseTodosService = TestBed.inject(MockFirebaseTodosService);

    }));

    it('Injected AuthService on the Component should be an instance of MockAuthService', () => {
        expect(authService instanceof MockAuthService).toBeTruthy();
    });

    it('Injected AngularFireAuth on the Component should be an instance of MockAngularFireAuthService', () => {
        expect(afAuth instanceof MockAngularFireAuthService).toBeTruthy();
    });

    it('Injected FirebaseTodosService on the Component should be an instance of MockFirebaseTodosService', () => {
        expect(fbDBService instanceof MockFirebaseTodosService).toBeTruthy();
    });


    it(
        // tslint:disable-next-line: max-line-length
        'Should create TodosComponent, having Redux Store and INJECTED Services (AuthService, AngularFireAuth and FirebaseTodosService) mocked up',
        () => { expect(thisComponent).toBeTruthy(); }
    );

    // Test 'x%' of the Component's METHODs, so test coverage will reach 'x%', on 'Functions()' column.
    describe('Should TodosComponent METHODs do what\'s expected, on: ', () => {
        const methodsToTest: Array<string> = ['onTodoCreation', 'onTodoManipulation', 'filterItemsList'];

        for (const thisMethod of methodsToTest) {
            switch (thisMethod) {
                case 'onTodoCreation':
                    it(`${thisMethod}() method: should call FirebaseTodosService.createTodo() method` , fakeAsync(() => {
                        // Spy on the Service's called Method, by this coponent's 'thisMethod' method:
                        const fakeNewTodoData: Promise<DocumentReference> = new Promise<DocumentReference>((resolve, reject) => resolve());
                        // tslint:disable-next-line: max-line-length
                        const spyOnCreateTodo = spyOn(useLessMockFirebaseTodosService, 'createTodo').and.returnValue(fakeNewTodoData);    // void
                        // Check initial setup:
                        expect(spyOnCreateTodo).not.toHaveBeenCalled();

                        // Check what's expected, if we call the component's method THROUGH our fake Service
                        // MIND YOU that that's what this component's 'thisMethod' method does!
                        useLessMockFirebaseTodosService.createTodo();
                        //
                        // Next is commented because would call component's 'onTodoCreation()',
                        // which we can't expect().toHaveBeenCalled() - on Jasmnin, you can only use it with spyes.
                        //
                        // So, unless this component's method would return ANYTHING, outside of the (any) Service call return,
                        // that we could expect() something out of it, we have, here, no ways to 'expect()' whatever...
                        // const newTodo = {} as Item;
                        // const firebaseUserId = '';
                        // const componentsMethodCall = thisComponent.onTodoCreation(newTodo, firebaseUserId);
                        // expect(componentsMethodCall).toHaveBeenCalledTimes(1);

                        // Check what's expected, once we spy on our 'useLessMockFirebaseTodosService' calling 'createTodo()' method:
                        expect(spyOnCreateTodo).toHaveBeenCalledTimes(1);
                        //
                        // Just for the sake of walking to the 100% coverage, just call myComponent's method:
                        const newTodo = {} as Item;
                        const firebaseUserId = '';
                        thisComponent.onTodoCreation(newTodo, firebaseUserId);
                        //
                        // It's NOT that simple - calllling COMPONENT's Method, you're triggering a REAL Store Service, which we haven't!
                        // Must go by the Spyes of a Fake Service:

                    }));
                    break;
                case 'onTodoManipulation':
                    describe(`• ${thisMethod}() method it should:`, () => {
                        // --------------------------------------------------
                        // Prepare a fake arg 'someDoneTodoUpdate',
                        // to use on the next it() callbacks args
                        // --------------------------------------------------
                        interface TodoObjToEdit {
                            action: string;
                            item: ItemWithId;
                            propToUpdate: { [key: string]: string | boolean };
                        }
                        // tslint:disable-next-line: max-line-length
                        const commonTodoItem: ItemWithId = { todo: 'TODO text', done: false, userId: 'someUserId123', autoID: 'I didn\'t made it!' };
                        //
                        const someDoneTodoUpdate: TodoObjToEdit = {
                            action: 'this prop should be replaced for each of the convenient\'s it() callbacks needed value',
                            item: commonTodoItem,
                            propToUpdate: { done: true}
                        };
                        // --------------------------------------------------

                        it(`call FirebaseTodosService.updateTodo() Method, if arg.action = 'update'` , () => {
                            const fakeTodoData: Promise<void> = new Promise<void>((resolve, reject) => resolve());
                            // tslint:disable-next-line: max-line-length
                            const spyOnUpdateTodo = spyOn(useLessMockFirebaseTodosService, 'updateTodo').and.returnValue(fakeTodoData);    // void
                            expect(spyOnUpdateTodo).not.toHaveBeenCalled();
                            useLessMockFirebaseTodosService.updateTodo({} as ItemWithId, {} as { [key: string]: string| boolean});
                            expect(spyOnUpdateTodo).toHaveBeenCalledTimes(1);
                            //
                            // Just for the sake of walking to the 100% coverage, just call myComponent's method:
                            someDoneTodoUpdate.action = 'update';
                            thisComponent.onTodoManipulation(someDoneTodoUpdate);
                        });

                        it(`call FirebaseTodosService.deleteTodo() Method, if arg.action = 'delete'` , () => {
                            const fakeTodoData: Promise<void> = new Promise<void>((resolve, reject) => resolve());
                            // tslint:disable-next-line: max-line-length
                            const spyOnDeleteTodo = spyOn(useLessMockFirebaseTodosService, 'deleteTodo').and.returnValue(fakeTodoData);    // void
                            expect(spyOnDeleteTodo).not.toHaveBeenCalled();
                            useLessMockFirebaseTodosService.deleteTodo({} as ItemWithId);
                            expect(spyOnDeleteTodo).toHaveBeenCalledTimes(1);
                            //
                            // Just for the sake of walking to the 100% coverage, just call myComponent's method:
                            someDoneTodoUpdate.action = 'delete';
                            thisComponent.onTodoManipulation(someDoneTodoUpdate);
                        });

                        it(`console.error(), if arg.action !== 'update' && !== 'delete'` , () => {
                            // Here we have a good example, whilst no Service is going to be called, on this component's Method branch
                            // (will enter on the 'default' branch of a switch()),
                            // to use the component, itself, and not a mocked Service, or a spy on it:
                            // --------------------------------------------------
                            // Prepare it - it wasn't that easy!
                            // --------------------------------------------------
                            // 1) mock an arg, for thisComponent's 'onTodoManipulation()' Method,
                            // where 'someDoneTodoUpdate.action' is NOT 'update', nor 'delete':
                            someDoneTodoUpdate.action = 'this action is NOT "update" and is NOT "delete"';
                            // tslint:disable-next-line: max-line-length
                            // 2) mock a 'mockedErrorFn' console.error, that will be triggered on the thisComponent's 'onTodoManipulation()' Method,
                            // if, and only if, 'someDoneTodoUpdate.action' is NOT 'update', nor 'delete'
                            let consoleOutput: string;
                            const mockedErrorFn: {
                                (...data: any[]): void;
                                (message?: any, ...optionalParams: any[]): void;
                            } = (output: string) => consoleOutput = output;
                            console.error = mockedErrorFn;
                            // --------------------------------------------------

                            // Now we can trigger the thisComponent's Method we want to expect() something - a console.error trigger:
                            const componentsMethodCall = thisComponent.onTodoManipulation(someDoneTodoUpdate);
                            expect(consoleOutput).toContain('Please code it, first!');
                            // and to reinforce this expect() is totally legit, though achieved through mocked params:
                            expect(consoleOutput).not.toContain('Please codeXXX it, first!');
                        });
                    });
                    break;

                case 'filterItemsList':
                    it(`${thisMethod}() method: should get a list of TODOs with only the "todo"s, only the "done"s or all of them` , () => {

                        const typeOfListToFilter = ['todos', 'dones', 'all', 'a non expected one'];
                        typeOfListToFilter.forEach((type: any) => {

                            thisComponent.filterItemsList(type);

                            // And then it's expected that:
                            switch (type) {
                                case 'todos':
                                    expect(thisComponent.filterDonePropTo).toBe(false);
                                    break;
                                case 'dones':
                                    expect(thisComponent.filterDonePropTo).toBe(true);
                                    break;
                                case 'all':
                                    expect(thisComponent.filterDonePropTo).toBeNull();
                                    break;

                                default:
                                    expect(thisComponent.filterDonePropTo).toBeNull();
                                    break;
                            }
                        });
                    });
                    break;

                default:
                    break;
            }
        }
    });
});
