import { DocumentReference, DocumentData  } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';

export interface Item {
    todo: string;
    done: boolean;
    userId: string;
}
// And then we'll need the AUTO ID (internal UID of each created collection's Object - a document - so it can be manipulated later):
export interface ItemWithId extends Item { autoID: string; }

export type DocumentChangeType = 'added' | 'modified' | 'removed';

// With the exception of the valueChanges(), each streaming method returns an Observable of DocumentChangeAction[].
export interface DocumentChangeAction {
    type: DocumentChangeType;    // <= what DocumentChangeType operation occured (added, modified, removed).
    payload: DocumentChange;     // <= important metadata about the change and a doc property which is the DocumentSnapshot.
}
export interface DocumentChange {
    type: DocumentChangeType;
    doc: DocumentSnapshot;
    oldIndex: number;
    newIndex: number;
}
export interface DocumentSnapshot {
    exists: boolean;
    ref: DocumentReference;
    id: string;
    metadata: firebase.firestore.SnapshotMetadata;
    data(): DocumentData;
    get(fieldPath: string): any;
}
