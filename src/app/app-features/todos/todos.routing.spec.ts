import { TestBed, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { of } from 'rxjs';

import { Router, RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot, NavigationExtras } from '@angular/router';
import { Location } from '@angular/common';
import { routes } from './todos.routing';

import { StoreModule, Store, MemoizedSelector, DefaultProjectorFn } from '@ngrx/store';
import { IMainAppModulesState, IAuthenticatedUser } from 'src/app/app-core/redux-store/store-pack.state.interfaces';

import { TodosComponent } from './todos.component';

import { AuthService } from '../../app-core/auth/firebase-auth.services';
// That will be called by any of the Router Guards:
import { AuthGuard } from 'src/app/app-core/auth/auth-guard.service';
import { AuthEmailGuard } from 'src/app/app-core/auth/auth-email-guard.service';
// And also to be Mocked Up:
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

// Our fake Router should be as close as possible to the Angular Router on the component's Routing list
class MockRouter implements Partial<Router> {
    navigate(commands: any[], extras?: NavigationExtras): Promise<boolean> {
        return {} as Promise<true>;
    }
}

// class MockAuthService {
//
// Better: we pretend that this fake class is as close as possible to AuthService
// but with fewer properties - we only want to know if User is logged in - and eventual if email is validated
class MockAuthService implements Partial<AuthService> {

    userIsLoggedIn: boolean;
    userHasValidatedEmail: boolean;

    constructor(
        isUserLoggedIn: boolean,
        isUserEmailValidated: boolean
    ) {
        this.userIsLoggedIn = isUserLoggedIn;
        this.userHasValidatedEmail = isUserEmailValidated;
    }

    // These are getters that, we, in fact, use on our AuthService:
    get isLoggedIn(): boolean { return this.userIsLoggedIn; }
    get isMailValidated(): boolean { return this.userHasValidatedEmail; }

    // On which Jasmine 'spyOn()' is NOT prepared to spy on (they use 'by comma' calls) => we need to pass it a Method, NOT a getter:
    isItLoggedIn(): boolean { return this.isLoggedIn; }
    isLoggedInEmailValidated(): boolean { return this.isMailValidated; }
}
class MockAngularFirestore implements Partial<AngularFirestore> { }

// We could have used also an "empty" Class here:
const MockAngularFireAuth = {
    auth: of({ uid: 'ABC123' })
};


describe('TodosRoutingModule', () => {
    let location: Location;
    let router: Router;
    let fixture: ComponentFixture<TodosComponent>;

    let fakeRouter: MockRouter;

    // tslint:disable-next-line: max-line-length
    // let fakeAuthService: MockAuthService;  // <= NOPES! Will not allow a "free" 'authGuard = new AuthEmailGuard(fakeAuthService, router);'
    let fakeAuthService: any;

    // This must be the real one - we want to check REAL 'authGuard.canActivate()' returning boolean
    let authGuard: AuthEmailGuard;
    let authGuardAnswer: boolean;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TodosComponent],

            // Our TodosComponent, uses a Redux Store injection on the constructor(),
            // and, obviously, the component being tested here (TodosRoutingModule) uses Angular Router:
            imports: [RouterTestingModule.withRoutes(routes), StoreModule.forRoot({})],

            // providers: [AuthEmailGuard, AuthGuard]  // <= No need... should be needed... I think... Well; it's not.
            providers: [                               // Google's Firebase Services that we depend on, for OUR Authoring of OUR Users:
                { provide: AngularFirestore, useClass: MockAngularFirestore },
                { provide: AngularFireAuth, useValue: MockAngularFireAuth }
            ]
        });

        fixture = TestBed.createComponent(TodosComponent);

        // ----------------------------------------------------------

        router = TestBed.inject(Router);
        fakeRouter = new MockRouter();

        location = TestBed.inject(Location);

        router.initialNavigation();
    });

    describe(`AuthEmailGuard 'canActivate()', for the protected routes,`, () => {
        describe(`should return TRUE for a LOGGED IN User, that has its EMAIL VERIFIED, on Google's Firebase,`, () => {
            beforeEach(() => {
                // Have to fake that the AuthService, which enquires Firebase asking:
                // "Hey you! Is this guy authored...?"
                // for Firebase to respond:
                // "Yes! Trust me! The "this.authService.isLoggedIn" Method is definetely, and fakely, returning TRUE, sir!"
                fakeAuthService = new MockAuthService(true, true);
                authGuard = new AuthEmailGuard(fakeAuthService, router);
                // So Angular's Router AuthEmailGuard alse checks to TRUE
                authGuardAnswer = authGuard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot);
            });

            it(`allowing '/todos' route to be called`, fakeAsync(() => {
                // router.navigate(['/todos']).then(() => {
                //     expect(location.path()).toBe('/todos');
                // });
                // Upper will go for the Angular component and, in fact, no AuthEmailGuard was defined HERE, on this spec file code.
                // We did have defined a FAKE one - but Jasmine ain't fool! :-)
                // Obviously Jasmine throws the error: "NullInjectorError: No provider for AuthEmailGuard!"
                //
                // So we 'spyOn([a fake router])', that has a REAL 'authGuard()' (that's what we're testing, right?)
                // questioning a FAKE Service and expect() what's possible on a 'fake' world - never on the real, component's one!
                const spyOnFakeNavigation = spyOn(fakeRouter, 'navigate');
                // And call it - onli now Angular Router, and testingm, have started
                // -------------------------------------------------
                const callTheSpy = fakeRouter.navigate(['/todos']);        // <= MIND YOU this is a 'fakeAsync()' it()
                // -------------------------------------------------
                if (authGuardAnswer === false) {
                    // Router shall re-direct User to the root Route:
                    fakeRouter.navigate(['/']);
                }
                // And see what's expected, regarding the 'canActivate()' Guard, that should be guarding this Route:
                expect(authGuardAnswer).toEqual(true);

                // And, if upper is in fact true, the User was NOT redirected to the (fake! ;-)) root Route but to the '/todos' one:
                expect(spyOnFakeNavigation).toHaveBeenCalledWith(['/todos']);
                expect(spyOnFakeNavigation).not.toHaveBeenCalledWith(['/']);
            }));

        });


        describe(`should return FALSE, even for a LOGGED IN User, that has its EMAIL NOT VERIFIED, on Google's Firebase,`, () => {
            beforeEach(() => {
                // Now we want the Service to return FALSE to the AuthEmailGuard enquire to it,
                // because Email wasn't verified by Firebase:
                //     like any ANONYMOUS login
                //     or email/password one, but that hasn't respond to the automatic email sent by Firebase.
                fakeAuthService = new MockAuthService(true, false);
                authGuard = new AuthEmailGuard(fakeAuthService, router);
                authGuardAnswer = authGuard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot);
            });

            it(`REDIRECTING the try to hii '/todos' GUARDED route to the '/' home page route`, fakeAsync(() => {
                // 1st: AuthEmailGuard says it's FALSE - no one was logged in, regarding the question answered by the FAKE AuthService:
                expect(authGuardAnswer).toEqual(false);

                // And then, regarding Angular Routing list:
                const spyOnFakeNavigation = spyOn(fakeRouter, 'navigate');
                fakeRouter.navigate(['/todos']);                            // <= MIND YOU this is a 'fakeAsync()' it()
                if (authGuardAnswer === false) {
                    fakeRouter.navigate(['/']);
                }
                expect(spyOnFakeNavigation).toHaveBeenCalledWith(['/']);

                // By the way:
                expect(spyOnFakeNavigation).not.toHaveBeenCalledTimes(0);
                expect(spyOnFakeNavigation).not.toHaveBeenCalledTimes(1);
                expect(spyOnFakeNavigation).toHaveBeenCalledTimes(2);
            }));
        });
    });

});
