// @ts-check
// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

// ----------------------
// Nice HTML report @
// https://medium.com/@gtgunarathna/test-result-report-configuration-for-protractor-test-suite-c36f58b7b616
// ----------------------
//
// Console on an HTML page:
// ----------------------
const { SpecReporter } = require('jasmine-spec-reporter');

const reportsDirectory = './coverage/e2e';
const detailsReportDirectory = reportsDirectory + '/protractor-jasmine2-screenshot-reporter';

var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');
const ScreenshotAndStackReporter = new HtmlScreenshotReporter({
    dest: detailsReportDirectory,
    filename: 'E2ETestingReport.html',
    reportTitle: "E2E Testing Report",
    showSummary: true,
    reportOnlyFailedSpecs: false,
    captureOnlyFailedSpecs: true,
});
// ----------------------

// Fancy stakeholders HTML beautified report
// ----------------------
var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');
const jasmineReporters = require('jasmine-reporters');
const { platform } = require('os');
const { now } = require('jquery');
const { browser } = require('protractor');

const dashboardReportDirectory = reportsDirectory + '/dashboard';
const picDirectory = dashboardReportDirectory + '/';
const date = new Date();
const nowsDateStr = date.toLocaleDateString() + '_' + date.toLocaleTimeString().replace(/:/g, '-').replace(/ /g, '_');
const picFileNameSfx = '-' + nowsDateStr + '-error.png'
// ----------------------


/**
 * @type { import("protractor").Config }
 */
exports.config = {
    allScriptsTimeout: 11000,
    directConnect: true,
    baseUrl: 'http://localhost:4200/',
    specs: [
        './src/**/*.e2e-spec.ts'
    ],
    capabilities: {
        chromeOptions: {
            // Run WITHOUT launching the browser
            // args: [ "--headless" ]
            // Watch over e2e launching - not easy! And still not solved by Angular team... there's no '--watch' thing.
            // https://github.com/angular/angular-cli/issues/2861#issuecomment-316972644
            // or
            // https://github.com/angular/angular-cli/issues/2861#issuecomment-581819533, could be a nice replacement...
        },
        'browserName': 'chrome',
    },
    framework: 'jasmine',
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000,
        print: function () { }
    },
    beforeLaunch: function () {
        return new Promise(function (resolve) {
            ScreenshotAndStackReporter.beforeLaunch(resolve);
        });
    },
    onPrepare() {
        require('ts-node').register({
            project: require('path').join(__dirname, './tsconfig.json')
        });
        jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));

        // Console on an HTML page:
        // ----------------------
        jasmine.getEnv().addReporter(ScreenshotAndStackReporter);

        // Fancy stakeholders HTML beautified report
        // ----------------------
        jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
            consolidateAll: true,
            savePath: dashboardReportDirectory + '/xml',
            filePrefix: 'xmlOutput'
        }));
        const fs = require('fs-extra');
        if (!fs.existsSync(dashboardReportDirectory)) {
            fs.mkdirSync(dashboardReportDirectory);
        }
        // Wanna add a pic of the browser IF e2e test didn't pass...?
        jasmine.getEnv().addReporter({
            specDone: (result) => {
                if (result.status == 'failed') {

                    this.browser.getCapabilities().then(function (caps) {
                        const browserName = caps.get('browserName');

                        this.browser.takeScreenshot().then(function (png) {
                            // const stream = fs.createWriteStream(dashboardReportDirectory + '/' + browserName + '-' + result.fullName + '.png');
                            const stream = fs.createWriteStream(picDirectory + browserName + picFileNameSfx);
                            stream.write(new Buffer(png, 'base64'));
                            stream.end();
                        });
                    });
                }
            }
        })
        // ----------------------

    },

    // STILL the fancy stakeholders HTML beautified report
    // ----------------------
    onComplete () {
        // const browserName, browserVersion;
        const capsPromise = this.browser.getCapabilities();

        capsPromise.then(function (caps) {
            const browserName = caps.get('browserName');
            const browserVersion = caps.get('version');
            const platform = caps.get('platform');

            const HTMLReport = require('protractor-html-reporter-2');
            const testConfig = {
                reportTitle: 'Protractor Test Execution Report',
                outputPath: dashboardReportDirectory,
                outputFilename: 'index',
                screenshotPath: './' + browserName + picFileNameSfx,
                testBrowser: browserName,
                browserVersion: browserVersion,
                modifiedSuiteName: false,
                screenshotsOnlyOnFailure: true,
                testPlatform: platform
            };
            new HTMLReport().from(reportsDirectory + '/dashboard/xml/xmlOutput.xml', testConfig);

            // Wanna keep the browser opened for 1 minute...?
            // browser.sleep(1 * 60 * 1000);
        });
    }
    // ----------------------
};