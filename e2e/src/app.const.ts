export const APP = {
    coreHomepage: {
        wrapperElem: {
            id: 'wrapper',
            toggleClass: 'toggled',
            coreHomepageLogin: {
                leftMenu: {
                    signUp: {
                        title: 'Welcome to XYZ!',
                        titleTag: 'h2',
                        msg: 'Please login either anonymously or by using one of the authorities below.',
                        msgTag: 'p',
                        authBtnsState: [
                            { buttonText: ' Gooogle', disabled: false },
                            { buttonText: ' GitHub', disabled: true },
                            { buttonText: ' Facebook', disabled: true },
                            { buttonText: ' Twitter', disabled: true },
                            { buttonText: ' Login Anonymously', disabled: false },
                        ],
                        xyzAuthForm: {
                            toSignIn: {
                                title: 'USER LOGIN',
                                titleTag: 'h4',
                                buttonText: 'Sign UP!',
                                inputEmailText: 'Email',
                                inputPasswordText: 'Pass',
                                formInputs: [
                                    { id: 'email', inputText: 'Email'},
                                    { id: 'password', inputText: 'Pass'}
                                ],
                                xyzSubmitButton: {
                                    buttonText: 'Submit'
                                }
                            },
                            toSignUp: {
                                title: 'NEW USER SIGNUP',
                                titleTag: 'h4',
                                buttonText: 'Sign IN!',
                                formInputs: [
                                    { id: 'name', inputText: 'Name'},
                                    { id: 'email', inputText: 'Email'},
                                    { id: 'password', inputText: 'Pass'}
                                ],
                                invalTexts: {
                                    name: {
                                        required: 'Name is required.'
                                        , minlength: 'Name must be at least 4 characters long.'
                                        , maxlength: 'Name cannot be more than 20 characters long.'
                                    }
                                    , email: {
                                        required: 'Email is required.'
                                        , email: 'Email must be a valid email'
                                    }
                                    , password: {
                                        required: 'Password is required.'
                                        , pattern: 'Password must include at least one letter and one number.'
                                        , minlength: 'Password must be at least 6 characters long.'
                                        , maxlength: 'Password cannot be more than 15 characters long.'
                                    }
                                },
                                xyzSubmitButton: {
                                    buttonText: 'Submit '
                                }
                            }
                        }
                    },
                    afterSignIn: {
                        title: 'Welcome back!',
                        titleTag: 'h2',
                        xyzUserCard: {
                            cardImgClass: 'card-img-container',
                            cardBodyClass: 'card-body',
                            cardFooter: {
                                class: 'card-footer',
                                cardButtons: [
                                    { buttonText: 'My Profile', disabled: false },
                                    { buttonText: 'Log Out', disabled: false },
                                ],
                            },
                            userEmailNotVerified: {
                                title: 'Warning',
                                titleTag: 'h4'
                            }
                        }
                    }
                }
            },
            topMenuBtnsState: [
                { buttonName: 'menu-trigger', disabled: false },
                { buttonName: 'homepage-trigger', disabled: false },
                { buttonName: 'profile-trigger', disabled: true },
                { buttonName: 'todos-trigger', disabled: true },
                { buttonName: 'void', disabled: true }
            ]
        }
    },

    coreHomepageHome: {
        titleTag: 'h1',
        title: 'Welcome to XYZ Reality!',
        iconsSrc: [
            { name: 'angular-logo', src: 'assets/images/angular.png' },
            { name: 'firebase-logo', src: 'assets/images/firebase.png' },
            { name: 'angular-firebase-logo', src: 'assets/images/angular-firebase.png' }
        ]
    },

    featureModules: [
        {
            name: 'todos',
            props: {
                btnsTo: {
                    create: {
                        triggerTextCreation: { buttonName: 'Create Todo', disabled: false },
                        submit: { buttonClass: 'xyz-todo-form-create button .fa-check-circle', disabled: false },
                        dismiss: { buttonClass: 'xyz-todo-form-create button .fa-times-circle-o', disabled: false }
                    },
                    edit: {
                        greenStateTogglerToDone: { buttonClass: 'xyz-todo-form-edit .btn-success', disabled: false },
                        redStateTogglerToTodo: { buttonClass: 'xyz-todo-form-edit .btn-danger', disabled: false },
                        triggerEditing: {
                            triggerTextEdition: { buttonClass: 'xyz-todo-form-edit button .fa-pencil-square-o', disabled: false },
                            submit: { buttonClass: 'xyz-todo-form-edit button .fa-check-circle', disabled: false },
                            dismiss: { buttonClass: 'xyz-todo-form-edit button .fa-times-circle-o', disabled: false }
                        },
                        delete: { buttonClass: 'xyz-todo-form-edit button .fa-trash', disabled: false }
                    },
                    write: {
                        onTodoCreationTextArea: { buttonClass: 'xyz-todo-form-create textarea', disabled: false },
                        onTodoEditingTextArea: { buttonClass: 'xyz-todo-form-edit textarea', disabled: false }
                    },
                    filter: {
                        toSeeOnlyTODOs: { buttonName: 'Todo(s)', disabled: false },
                        toSeeOnlyDONEs: { buttonName: 'Done(s)', disabled: false },
                        toSeeThemAll: { buttonName: 'All', disabled: false }
                    }
                }
            }
        }
    ],
};
