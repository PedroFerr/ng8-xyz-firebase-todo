import { async } from '@angular/core/testing';
import { promise, WebElementPromise  } from 'selenium-webdriver';
import { browser, logging, element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { AppPage } from './app.po';
import { APP } from './app.const';

import { coreHomepageHomeComponentE2E } from './e2e-spec-lib/core-homepage-home.e2e-spec';
import { coreHomepageLoginComponentE2E } from './e2e-spec-lib/core-homepage-login.e2e-spec';
import { xyzButtonsFlexBarComponentE2E } from './e2e-spec-lib/xyz-buttons-flex-bar.e2e-spec';

export const BROWSER_PAUSE = 1000;    // in 'millisecs'

describe('App', () => {

    let page: AppPage;

    beforeEach(() => {
        page = new AppPage();
        // ========================================
        browser.waitForAngularEnabled(false);
        // ========================================
        // If you forget up... after a click that fetches new content behond the App
        // .... my friend... nothing, absolutely NOTHING will EVER be expected, after!
        // ... and everything will be timeouts, failing ANY expect().
        // ========================================
        // https://stackoverflow.com/a/46532400/2816279
        // ========================================
        // But this way you'll be loosing a few specs...
        //
        // OR
        //
        // Set it CONVENIENTLY to false BEFORE fetching data outside and set it back to true AFTER affairs have finished
        // Check it on the Angular / Protractor Github: https://github.com/angular/protractor/blob/master/docs/timeouts.md
        // Welll... still does NOT work.
        // Better forget it for good and set 'browser.waitForAngularEnabled(false);' right at the 1st 'beforeEach()'.
    });

    it(`should display on an ${APP.coreHomepageHome.titleTag} tag the welcome message "${APP.coreHomepageHome.title}"`, () => {
        page.navigateTo();
        // expect(page.getTitleText()).toEqual(APP_TITLE);
        // Testing some generic function, @ 'e2e\src\app.po.ts', for headings on different "h" tags:
        page.expectText(APP.coreHomepageHome.titleTag, APP.coreHomepageHome.title);
    });

    describe(`has |core-homepage-home| landing page component`, () => coreHomepageHomeComponentE2E());

    describe(`has |xyz-buttons-flex-bar| button's top MENU bar UI/UX library component`, () => xyzButtonsFlexBarComponentE2E());

    describe(`has |core-homepage-login| left side MENU component`, () => coreHomepageLoginComponentE2E());

    // Wanna keep the browser opened for 10 secs, fter all testing is done...? To really see clickings and App's messages...?
    // MIND YOU:
    // this pause can NOT be bigger than 'jasmine.DEFAULT_TIMEOUT_INTERVAL' - test will stop by breaking...
    it(`(${BROWSER_PAUSE / 1000} secs pause, before closing the browser)`, () => browser.sleep(BROWSER_PAUSE));


    // Produce the report.
    // (currently - see 'e2e\protractor.conf.js' set up - we are producing, besides the default Jasmin console one, 2 riched HTML reports)
    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        expect(logs).not.toContain(jasmine.objectContaining({
            level: logging.Level.ALL,
        } as logging.Entry));
    });
});
