import { browser, logging, element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { AppPage } from '../app.po';

import { APP } from '../app.const';
import { TODOActionElements, BtnsNameState } from '../app.e2e-spec.interfaces';

import { BROWSER_PAUSE } from '../app.e2e-spec';

/**
 * MIND YOU:
 * This is a brand new 'describe()'
 * triiggered on |xyz-auth-form| E2E spec @ 'e2e\src\e2e-spec-lib\xyz-auth-form.e2e-spec.ts'
 *
 * REMEMEBR we do need to have someone LOGGED IN,
 * either by name/password or by any world known giant allowed authoring brand,
 * to have acces/rights to CRUD and LIST/FIlter TODOs)
 *
 * Todo so, on |xyz-auth-form|, we have authored a User into this tests browser, using the following credentials:
 * const inputedSignInValues: Array<string> = ['PedroFerr@Hotmail.com', '123456789a'];
 *
 */
export const featureTodosComponentE2E = () => {
    let page: AppPage;
    let actionTriggers: TODOActionElements;

    beforeEach(() => {
        page = new AppPage();
        actionTriggers = page.getActionTrigers();
    });


    it(`should CREATE a 'todo'`, () => {
        const newTodoText = 'This is an automated Protractor CREATION of a TODO.\n' +
            'Just to remind you have to finish and deliver TODAY the App\'s E2E Testing!'
        ;

        actionTriggers.create.triggerTextCreation().click();
        browser.sleep(BROWSER_PAUSE / 3);

        actionTriggers.write.onTodoCreationTextArea().sendKeys(newTodoText);
        browser.sleep(BROWSER_PAUSE / 3);

        actionTriggers.create.submit().click();
        browser.sleep(BROWSER_PAUSE);
    });

    it(`should UPDATE a 'todo' text`, () => {
        const newTodoTextEdited = 'This is an automated Protractor EDITION of a TODO.\n' +
            'Just to remind you - don\'t need! - have to finish and deliver TODAY the App\'s E2E Testing!' +
            '\nThanks!\nThat was handy!'
        ;

        actionTriggers.edit.triggerEditing.triggerTextEdition().click();
        browser.sleep(BROWSER_PAUSE / 3);

        actionTriggers.write.onTodoEditingTextArea().clear();
        browser.sleep(BROWSER_PAUSE / 3);

        actionTriggers.write.onTodoEditingTextArea().sendKeys(newTodoTextEdited);
        browser.sleep(BROWSER_PAUSE / 3);

        actionTriggers.edit.triggerEditing.submit().click();
        browser.sleep(BROWSER_PAUSE);
    });

    it(`should toggle a 'todo' TODO state into DONE, and vice-versa`, () => {
        // Transform a TODO into a DONE:
        actionTriggers.edit.greenStateTogglerToDone().click();
        browser.sleep(BROWSER_PAUSE);

        // Transform a DONE into a TODO, again:
        actionTriggers.edit.redStateTogglerToTodo().click();
        browser.sleep(BROWSER_PAUSE);
    });

    it(`should DELETE a 'todo', having a confirmation dialogue popup (accept)`, () => {
        actionTriggers.edit.delete().click();
        browser.sleep(BROWSER_PAUSE);
        browser.switchTo().alert().accept();
        browser.sleep(BROWSER_PAUSE);
    });

    describe(`and should be able to FILTER the TODOS list,`, () => {

        const filterBtnsName: { [key: string]: BtnsNameState} = APP.featureModules[0].props.btnsTo.filter;

        it(`by clicking on one of the 'My TODOs Feed' bar of 3 colored buttons`, () => {
            expect(element(by.css('.filter-buttons')).isPresent).toBeTruthy();
            expect(element.all(by.css('.filter-buttons button')).count()).toEqual(3);
        });

        it(`should filter to TODOs, by cllicking on #0 '${filterBtnsName.toSeeOnlyTODOs.buttonName}' button`, () => {
            actionTriggers.filter.toSeeOnlyTODOs().click();
            browser.sleep(BROWSER_PAUSE);
        });

        it(`should filter to DONEs, by cllicking on #1 '${filterBtnsName.toSeeOnlyDONEs.buttonName}' button`, () => {
            actionTriggers.filter.toSeeOnlyDONEs().click();
            browser.sleep(BROWSER_PAUSE);
        });

        it(`should unfilter ALL, by cllicking on #2 '${filterBtnsName.toSeeThemAll.buttonName}' button`, () => {
            actionTriggers.filter.toSeeThemAll().click();
            browser.sleep(BROWSER_PAUSE);
        });

        it(`HURRAY! User will go home! So... FINALLY back to the begining:`, () => {
            //
        });

    });
};

