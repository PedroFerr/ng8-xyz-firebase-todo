import { browser, logging, element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { AppPage } from '../app.po';

import { featureTodosComponentE2E } from './feature-todos.e2e-spec';

import { FormInputId } from '../app.e2e-spec.interfaces';
import { BROWSER_PAUSE } from '../app.e2e-spec';

const page = new AppPage();

export const xyzAuthFormComponentE2E = (signUpState: any, afterSignInState: any) => {
    // -----------------------------------------------------------------------
    const leftMenuXyzAuthForm: any = signUpState.xyzAuthForm;
    // -----------------------------------------------------------------------
    const toSignINstate: any = leftMenuXyzAuthForm.toSignIn;
    const toSignUPstate: any = leftMenuXyzAuthForm.toSignUp;

    const formInputs: Array<FormInputId> = toSignUPstate.formInputs;
    const invalidInputMsgs = toSignUPstate.invalTexts;

    // -----------------------------------------------------------------------

    const formInputsForSignIn: Array<FormInputId> = toSignINstate.formInputs;
    const inputedSignInValues: Array<string> = ['PedroFerr@Hotmail.com', '123456789a'];

    // -----------------------------------------------------------------------
    // To last log out, ending the Suite of E2E Tests:
    const userProfileBtns = afterSignInState.xyzUserCard.cardFooter.cardButtons;
    const loggOutBtn = page.getButtonByButtonText(userProfileBtns[1].buttonText);

    // -----------------------------------------------------------------------


    it(`should be RENDERED, containing an HTML form with more tags. Those are:`, () => {
        expect(element(by.binding('xyz-auth-form form')).isPresent).toBeTruthy();
    });

    it(`an '${toSignINstate.titleTag}' tag supporting an welcome badge like "${toSignINstate.title}"`, () => {
        page.expectText(toSignINstate.titleTag, toSignINstate.title);
    });
    it(`1 Sign UP/IN toggler button, 2 inputs and a |xyz-submit-button| component's button to submit the form`, () => {
        expect(element(by.buttonText('Sign UP!')).isPresent()).toBeTruthy();

        expect(element(by.id('email')).isPresent).toBeTruthy();
        expect(element(by.id('password')).isPresent).toBeTruthy();

        expect(page.getElementByCss('xyz-submit-button button').isPresent).toBeTruthy();
    });

    it(`should be able to allow a new SIGN UP, by clicking on Sign UP/IN toggler button`, () => {
        page.getButtonByButtonText(toSignINstate.buttonText).click();
        // MIND YOU:
        // Once clicked, the button TOGGLEd => it's, now, a sign IN button (and not UP anymore) => different text!
    });

    page.expectedUserToHaveBeenSignedUp(toSignUPstate, formInputs, invalidInputMsgs, afterSignInState);

    it(`Nevertheless, should NOT be allowed, after User Signs OUT, to do a new Sign UP with the SAME credentials`, () => {
        // TODO:
        // It does not; that's true - because Firebase won't let that happen.
        //
        // But, on the Application, the only error/alert that is launched is a console.error - no UI/UX!
        // Transform that in some kind of alert/modal, warning the User those credentials already exist...
        //
        // Then.... makes sense next it(), for now commented.
    });
    // it(`It should render "" error Message `, () => {

    // });

    it(
        `|xyz-auth-form| should allow a REGISTERED (email/password) user, with VERIFIED email by Google's Firebase, to SIGN IN`,
        () => {
            inputedSignInValues.forEach((inputRealValue: string, idx: number) => {
                page.getInputById(formInputsForSignIn[idx].id).sendKeys(inputRealValue);
                browser.sleep(BROWSER_PAUSE / 3);
            });

            expect(page.getElementByCss('form').getAttribute('class')).toContain('ng-valid');    // 'ng-touched ng-dirty ng-invalid'

            page.getElementByCss('xyz-submit-button button').click();
            browser.sleep(BROWSER_PAUSE);

            // Let's forget the verification User is in fact logged in calling
            // 'page.expectedUserToHaveBeenLoggedIn(afterSignInState, true, false);'
            // - already made for Anonymous, and for a fresh new signed up user. Enough.
        }
    );

    it(`and should be able to navigate to App's Feature Module "TODO" @ '/todos'`, () => {
        page.getTopMenuBarBtns().get(3).click().then(() => {
            expect(browser.getCurrentUrl()).toContain('/todos');
        });
        browser.sleep(BROWSER_PAUSE);
    });

    describe(`has |feature-todos| App's Feature MODULE "TODOS" able to be CRUDed, `, () => {
        featureTodosComponentE2E();
        // And, when upper will return, we shall log out current E2E automated User:
        page.expectedUserToBeLoggedOut(signUpState, loggOutBtn);
    });


};
