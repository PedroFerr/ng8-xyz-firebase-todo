import { browser, logging, element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { AppPage } from '../app.po';
import { APP } from '../app.const';

import { xyzAuthFormComponentE2E } from './xyz-auth-form.e2e-spec';

import { BtnsTextState } from '../app.e2e-spec.interfaces';
import { BROWSER_PAUSE } from '../app.e2e-spec';

const page = new AppPage();

export const coreHomepageLoginComponentE2E = () => {

    // -----------------------------------------------------------------------
    const leftMenu: any = APP.coreHomepage.wrapperElem.coreHomepageLogin.leftMenu;

    const leftMenuAuthBtns = {
        btns: () => page.getLeftMenuAuthBtns(),
        totalBtns: () => page.getTotalLeftMenuAuthBtns()
    };
    const signUpState = leftMenu.signUp;
    const authBtnsState: Array<BtnsTextState> = signUpState.authBtnsState;
    const anonymouslyLoginBtnIdx = 4;

    const afterSignInState = leftMenu.afterSignIn;
    // -----------------------------------------------------------------------

    it(
        `should be RENDERED with an '${signUpState.titleTag}' tag supporting an welcome badge like "${signUpState.title}"` +
        `and, underlyng, a "${signUpState.msg}" log in hint, on a '${signUpState.msgTag}' tag`
        , () => page.expectText(signUpState.titleTag, signUpState.title)
    );
    it(`and containing ${authBtnsState.length} different AUTH authorities buttons. Those are:,`, () => {
        expect(leftMenuAuthBtns.btns().isPresent()).toBeTruthy();
        expect(leftMenuAuthBtns.totalBtns()).toBe(authBtnsState.length);
    });
    // The AUTHORING world-known-major authoriites different colorful buttons:
    authBtnsState.forEach((btnState: {buttonText: string; disabled: boolean}, idx: number) => {
        it(
            ` --- #${idx}, "${btnState.buttonText.trimLeft()}" button is "${btnState.disabled === true ? 'DISABLED' : 'ENABLED'}"`,
            () => {
                // Check each btn has the given buttonName on 'topMenuBtnsState':
                expect(element(by.buttonText(btnState.buttonText)).isPresent).toBeTruthy();
                // ... and, after ngOnInit(), if it's expected to be disabled or not:
                page.expectedLeftMenuAuthDisabledBtns(idx, btnState.disabled);
            }
        );
    });

    it(`that should OPEN Google's auth form, by clicking on #0, "${authBtnsState[0].buttonText.trimLeft()}" button`, () => {
        leftMenuAuthBtns.btns().get(0).click();
    });
    // IF you wanted to test the log in ACTION, by supplying some email and password onto the Google's form popup,
    // we should have a Google Auth page object - check it @ https://stackoverflow.com/a/33831771/2816279
    // Then treat it as any normal login:
    // -------------------------------------
    //   loginPage.connectWithGoogleBtn.click();
    //   googlePage.loginToGoogle();
    //   browser.wait(mainPage.userName.isPresent()).
    //   then(function () {
    //     expect(mainPage.userName.getText()).
    //     toEqual('my.account@google.com');
    //     done();
    //   });
    // -------------------------------------
    // The key here is "isAngularSite(false);" function witch instructs webdriver
    // wether you're entering a 'non angular' Webite (Google's form submission) or coming back to our App's 'angular' website.
    //
    // We are not going to do it due to obvious reasons of exposing a Google's log in credentials data.
    // We could, in fact, use the '.env' plug in dependency, to use some 'secret' login and password that would rest on this machine...
    // But, anyway, after Google's popup is open, what happens till a login is successfully, or not, has not to do with this App code.
    //
    // To test a successful, or not, login, we'll use the 'Anonymous' button/login, further in front - though also depending on a
    // Firebase service, what happens before, and after, submiting the credentials, afects this App in the same way - and UI/UX.
    //
    it(
        `OR that should LOG us IN, through ANONYMOUSLY credentials, by clicking on ` +
        `#${anonymouslyLoginBtnIdx}, "${authBtnsState[anonymouslyLoginBtnIdx].buttonText.trimLeft()}" button,` +
        `taking us to the User area, where`,
        () => {
            leftMenuAuthBtns.btns().get(anonymouslyLoginBtnIdx).click();
            browser.sleep(BROWSER_PAUSE);
        }
    );

    // And the proof someone, in fact, has logged in, is the SAME for any kind of authoring (Google, Anonymously or mail/password)
    // EXCEPT if the User email is still NOT verified, on Firebase. If that is case, the difference is that
    // User should NOT have access to App's Feature Module TODOS, by the '/todos' Angular's Router route - 2nd param to 'false'
    page.expectedUserToHaveBeenLoggedIn(afterSignInState, false, true);
    // Upper Method call should do ALL the E2E tests, proofing User IS logged in, AND log him/her OUT, automatic and immediatelly
    // - 3rd param to 'true'

    describe(
        `And, finally, the |xyz-auth-form| bottom's UI/UX library component, for User's SIGN UP/IN and LOG OUT,`,
        () => xyzAuthFormComponentE2E(signUpState, afterSignInState)
    );

};
