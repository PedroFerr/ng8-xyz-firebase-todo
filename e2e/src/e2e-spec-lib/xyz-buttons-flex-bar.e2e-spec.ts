import { browser, logging, element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { AppPage } from '../app.po';
import { APP } from '../app.const';

import { BROWSER_PAUSE } from '../app.e2e-spec';

const page = new AppPage();

export const xyzButtonsFlexBarComponentE2E = () => {

    // -----------------------------------------------------------------------
    const wrapperElemId: string = APP.coreHomepage.wrapperElem.id;
    const toggleClass: string = APP.coreHomepage.wrapperElem.toggleClass;
    // -----------------------------------------------------------------------
    const topMenu = {
        btns: () => page.getTopMenuBarBtns(),
        totalBtns: () => page.getTotalTopMenuBarBtns()
    };
    const topMenuBtnsState = APP.coreHomepage.wrapperElem.topMenuBtnsState;
    // -----------------------------------------------------------------------

    it(`should be RENDERED, containing ${topMenuBtnsState.length} buttons. Those are:`, () => {
        expect(topMenu.btns().isPresent()).toBeTruthy();
        expect(topMenu.totalBtns()).toBe(topMenuBtnsState.length);
    });

    // You can't do a 'forEach()' cycle on 'topMenu.btns()' - it's an ElementArrayFinder, NOT an Array<any>!
    topMenuBtnsState.forEach((btnState: {buttonName: string; disabled: boolean}, idx: number) => {
        it(
            ` --- #${idx}, "${btnState.buttonName}" button is "${btnState.disabled === true ? 'DISABLED' : 'ENABLED'}"`,
            () => {
                // Check each btn has the given buttonName on 'topMenuBtnsState':
                expect(element(by.binding(btnState.buttonName)).isPresent).toBeTruthy();
                // ... and, after ngOnInit(), if it's expected to be disabled or not:
                page.expectedTopMenuBarDisabledBtns(idx, btnState.disabled);
            }
        );
    });

    it(
        `that should OPEN |core-homepage-login| left side MENU component, by clicking on #0, "${topMenuBtnsState[0].buttonName}" button`,
        () => topMenu.btns().get(0).click()
    );
    it(`=> |core-homepage| component's "#${wrapperElemId}" element should, now, have class "${toggleClass}"`, () => {
        const coreHomePageWrapper: ElementFinder = element(by.id(wrapperElemId));
        expect(coreHomePageWrapper.getAttribute('class')).toContain(toggleClass);
        browser.sleep(BROWSER_PAUSE);
    });


    it(
        `that should CLOSE |core-homepage-login| left side MENU component, by clicking on #0, "${topMenuBtnsState[0].buttonName}" button`,
        () => topMenu.btns().get(0).click()
    );
    it(`=> |core-homepage| component's "#${wrapperElemId}" element should, now, NOT have class "${toggleClass}"`, () => {
        const coreHomePageWrapper: ElementFinder = element(by.id(wrapperElemId));
        expect(coreHomePageWrapper.getAttribute('class')).not.toContain(toggleClass);
        browser.sleep(BROWSER_PAUSE);
    });


    it(
        `that should OPEN |core-homepage-login| left side MENU component, by clicking on #0, "${topMenuBtnsState[0].buttonName}" button`,
        () => topMenu.btns().get(0).click()
    );
    it(`=> |core-homepage| component's "#${wrapperElemId}" element should, now, have AGAIN class "${toggleClass}"`, () => {
        const coreHomePageWrapper: ElementFinder = element(by.id(wrapperElemId));
        expect(coreHomePageWrapper.getAttribute('class')).toContain(toggleClass);
    });
};
