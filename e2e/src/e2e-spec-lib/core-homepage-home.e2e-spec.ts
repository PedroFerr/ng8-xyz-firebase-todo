import { browser, logging, element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { AppPage } from '../app.po';
import { APP } from '../app.const';

import { ImgSrcAndName } from '../app.e2e-spec.interfaces';

const page = new AppPage();

export const coreHomepageHomeComponentE2E = () => {
    // Always, always, always BEFORE any Protractor query!
    // If not we'll get the error: "Error while waiting for Protractor to sync with the page"
    // https://github.com/angular/protractor/issues/2643#issuecomment-302247135
    // If it keeps happening, maybe addding 'browser.waitForAngularEnabled(false);' could work...
    // https://github.com/angular/protractor/issues/2643#issuecomment-401661396
    // or
    // https://stackoverflow.com/a/54979237/2816279
    // ---------------
    // browser.get('/');
    // ---------------
    // Apparentely... NOPES - could this 'rule' be of ald times? AngularJS times...?
    // If you apply it, you clearly see the browser (top) refreshing, each time 'browser.get('/');' is called.

    const iconImgs: Array<ImgSrcAndName> = APP.coreHomepageHome.iconsSrc;

    it(`should be RENDERED, containing ${iconImgs.length} images/icons. Those are`, () => {
        expect(page.getTotalNumberImgs()).toBe(iconImgs.length);
    });

    iconImgs.forEach( (eachImg: ImgSrcAndName, idx: number) => {
        it(
            ` --- #${idx}, "${eachImg.name}" image/icon`,
            () => {
                const imgElem: ElementFinder = element(by.css(`img[src*='${eachImg.src}']`));
                expect(imgElem.isPresent()).toBeTruthy();
            }
        );
    });
};
