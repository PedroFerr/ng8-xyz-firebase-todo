import { browser, by, element, ElementFinder, ElementArrayFinder } from 'protractor';

import { APP } from './app.const';
import { BtnsNameState, FormInputId, TODOActionElements } from './app.e2e-spec.interfaces';

import { BROWSER_PAUSE } from './app.e2e-spec';

export class AppPage {

    navigateTo(): Promise<any> {
        return browser.get(browser.baseUrl) as Promise<any>;
    }

    getTitleText(): Promise<string> {
        return element(by.css('app-root h1')).getText() as Promise<string>;
    }

    expectText(tag: string, expectedText: string): void {
        const hText = element(by.css(tag)).getText() as Promise<string>;
        expect(hText).toEqual(expectedText);
    }

    // ------------------------------------------
    // |xyz-buttons-flex-bar| component E2E spec
    // Top Menu Bar buttons get()'ers and expectedTo...()'s
    // ------------------------------------------

    getTopMenuBarBtns(): ElementArrayFinder {
        return element.all(by.css('.buttons-flex-bar button'));
    }

    // tslint:disable-next-line: max-line-length
    getTotalTopMenuBarBtns(): Promise<number> {  // check Promises Type: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/15215#issuecomment-287278193
        return this.getTopMenuBarBtns().count() as Promise<number>;
    }

    expectedTopMenuBarDisabledBtns(btnIdx: number, toBeDisabled: boolean): void {
        const topBarBtns = this.getTopMenuBarBtns();

        if (toBeDisabled === true) {
            expect(topBarBtns.get(btnIdx).isEnabled()).toBeFalsy();
        } else {
            expect(topBarBtns.get(btnIdx).isEnabled()).toBeTruthy();
        }
    }

    // ------------------------------------------
    // |core-homepage-Home| component E2E spec
    // get()'ers and expectedTo...()'s
    // of images / icons and Authoring buttons
    // ------------------------------------------

    getTotalNumberImgs(): Promise<number> {
        return element.all(by.css('img')).count() as Promise<number>;
    }

    getLeftMenuAuthBtns(): ElementArrayFinder {
        return element.all(by.css('.auth-buttons-container button'));
    }

    // tslint:disable-next-line: max-line-length
    getTotalLeftMenuAuthBtns(): Promise<number> {  // check Promises Type: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/15215#issuecomment-287278193
        return this.getLeftMenuAuthBtns().count() as Promise<number>;
    }

    expectedLeftMenuAuthDisabledBtns(btnIdx: number, toBeDisabled: boolean): void {
        const topBarBtns = this.getLeftMenuAuthBtns();

        if (toBeDisabled === true) {
            expect(topBarBtns.get(btnIdx).isEnabled()).toBeFalsy();
        } else {
            expect(topBarBtns.get(btnIdx).isEnabled()).toBeTruthy();
        }
    }

    // ------------------------------------------
    // |xyz-auth-form| component E2E spec
    // get()'ers and expectedTo...()'s
    // of general elements, inputs, validation msgs, etc.
    // ------------------------------------------

    getElementByCss(cssTag: string): ElementFinder {
        return element(by.css(cssTag));
        // return element(by.binding('form'));
        //
        // MIND YOU:
        // Upper, though should be CORRECT, it's another BUG from Angular/Protractor:
        // "Protractor works with AngularJS versions greater than 1.0.6/1.1.4, and is COMPATIBLE with Angular applications.
        // "Note that for Angular apps, the by.binding() and by.model() locators are not supported. We recommend using by.css()."
        // https://stackoverflow.com/a/44558348/2816279
    }

    getButtonByButtonText(buttonText: string): ElementFinder {
        return element(by.buttonText(buttonText));
    }

    getInputById(byId: string): ElementFinder {
        return element(by.id(byId));
    }


    expectedInvalidInputHTMLMsgFor(byId: string, expectedText: string): void {
        let notificationClassMemmberIdx = -1;
        switch (byId) {
            case 'name':
                notificationClassMemmberIdx = 0;
                break;

            case 'email':
                notificationClassMemmberIdx = 1;
                break;

            case 'password':
                notificationClassMemmberIdx = 2;
                break;

            default:
                break;
        }
        const thisInvalidInputHTMLMsg: ElementFinder = element.all(by.css('.notification')).get(notificationClassMemmberIdx);
        // And so, get it's text compare it, and do nothing more - Protractor will act, on an expect(), even on a "null" one:
        expect(thisInvalidInputHTMLMsg.getText()).toEqual(expectedText);
    }

    // ------------------------------------------
    // |feature-todos| component E2E spec
    // Mainly, a HUGE get()'er of Promise resolvers
    // of ElementFinder's, to pass to the component spec.
    // ------------------------------------------
    getActionTrigers(): TODOActionElements {
        const todoFeatureModule = APP.featureModules[0];

        const createBtns = todoFeatureModule.props.btnsTo.create;
        const editBtns = todoFeatureModule.props.btnsTo.edit;
        const writeBtns = todoFeatureModule.props.btnsTo.write;
        const filterBtns = todoFeatureModule.props.btnsTo.filter;

        return {
            create: {
                triggerTextCreation: () => element(by.buttonText(createBtns.triggerTextCreation.buttonName)),
                submit: () => element(by.css(createBtns.submit.buttonClass)),
                dismiss: () => element(by.css(createBtns.dismiss.buttonClass))
            },
            edit: {
                greenStateTogglerToDone: () => element(by.css(editBtns.greenStateTogglerToDone.buttonClass)),
                redStateTogglerToTodo: () => element(by.css(editBtns.redStateTogglerToTodo.buttonClass)),
                triggerEditing: {
                    triggerTextEdition: () => element(by.css(editBtns.triggerEditing.triggerTextEdition.buttonClass)),
                    submit: () => element(by.css(editBtns.triggerEditing.submit.buttonClass)),
                    dismiss: () => element(by.css(editBtns.triggerEditing.dismiss.buttonClass)),
                },
                delete: () => element(by.css(editBtns.delete.buttonClass)),
            },
            write: {
                onTodoCreationTextArea: () => element(by.css(writeBtns.onTodoCreationTextArea.buttonClass)),
                onTodoEditingTextArea: () => element(by.css(writeBtns.onTodoEditingTextArea.buttonClass))
            },
            filter: {
                toSeeOnlyTODOs: () => element(by.buttonText(filterBtns.toSeeOnlyTODOs.buttonName)),
                toSeeOnlyDONEs: () => element(by.buttonText(filterBtns.toSeeOnlyDONEs.buttonName)),
                toSeeThemAll: () => element(by.buttonText(filterBtns.toSeeThemAll.buttonName)),
            }
        };
    }

    // ------------------------------------------
    // General EXPECTED cases:
    // Force User to SignUp, Login, Logout, etc.
    // ------------------------------------------

    /**
     * If next E2E Tests all go green, this is the proof someone, in fact, has logged in.
     * And it's the SAME for any kind of authoring (Google, GitHub, Twitter, etc., Anonymously or mail/password).
     *
     * Only EXCEPTION occurs if the logged in User email is still NOT verified, on Firebase.
     * In this case the User should NOT have access to App's Feature Module TODOS, by the '/todos' Angular's Router route.
     *
     * So, calling this Method of the 'AppPage', @hasEmailVerified should be passed in as FALSE,
     * whenever we want an ANONYMOUS User, or a User that has just Sign UP and didn't, yet, respond to a Frebase form, from it's own email,
     * allowing Google to verify its own email, to Log IN into the Application.
     *
     * @param afterSignInState - the Apps State (params @ 'e2e\src\app.const.ts') AFTER User has signed in
     * @param hasEmailVerified - should be passed in as TRUE, only when User is authored by an world known authorized source
     *                             like Google, GitHub, Twitter, etc.
     *                             or by its own email/password AND has its email verified by Firebase
     * @param shallUserBeLoggedOut - after test was done proofing User is in fact logged in,
     *                             User will be logged out automatic and immediatelly.
     */
    expectedUserToHaveBeenLoggedIn(afterSignInState: any, hasEmailVerified: boolean, shallUserBeLoggedOut: boolean): void {
        // -----------------------------------------------------------------------
        const topMenu = {
            btns: () => this.getTopMenuBarBtns(),
            totalBtns: () => this.getTotalTopMenuBarBtns()
        };
        const topMenuBtnsState: Array<BtnsNameState> = APP.coreHomepage.wrapperElem.topMenuBtnsState;

        // -----------------------------------------------------------------------

        const userProfileCard = afterSignInState.xyzUserCard;
        const userProfileBtns = userProfileCard.cardFooter.cardButtons;
        const myProfileBtn: ElementFinder = this.getElementByCss('.card-footer a');
        const logOutBtn: ElementFinder = element(by.buttonText(userProfileBtns[1].buttonText));

        // -----------------------------------------------------------------------

        const leftMenu: any = APP.coreHomepage.wrapperElem.coreHomepageLogin.leftMenu;
        const signUpState = leftMenu.signUp;

        it(
            `should be RENDERED an '${afterSignInState.titleTag}' tag supporting an welcome BADGE like "${afterSignInState.title}"`,
            () => {
                this.expectText(afterSignInState.titleTag, afterSignInState.title);
            }
        );
        it(`along with an UI/UX library |xyz-user-card| component User Profile Card, with an User image, a body and footer`, () => {
            const anyUserProfileImg: ElementFinder = this.getElementByCss('img');
            expect(anyUserProfileImg.isPresent()).toBeTruthy();
            expect(this.getElementByCss(`.${userProfileCard.cardImgClass}`).isPresent()).toBeTruthy();
            expect(this.getElementByCss(`.${userProfileCard.cardBodyClass}`).isPresent()).toBeTruthy();
            expect(this.getElementByCss(`.${userProfileCard.cardFooter.class}`).isPresent()).toBeTruthy();
        });

        it(`having also rendered, on footer, a pair of 'MY PROFILE' and a 'LOG OUT' buttons`, () => {
            // 'My Profile' is an <a href /> and Protractor sees it with capital letters:
            const myProfileAHrefText: string = userProfileBtns[0].buttonText.toUpperCase();
            expect(myProfileBtn.isPresent()).toBeTruthy();
            expect(myProfileBtn.getTagName()).toBe('a');
            expect(myProfileBtn.getText()).toBe(myProfileAHrefText);
            // and 'Log Out' is, in fact, a <button /> - and Protractor sees it with... small letters! ;-)
            expect(logOutBtn.isPresent()).toBeTruthy();
        });

        it(`
            'MY PROFILE' button click, should navigate the User to its '/profile' route, at the right side of the User Profile Card`,
            () => {
                myProfileBtn.click().then(() => {
                    expect(browser.getCurrentUrl()).toContain('/profile');
                });
                browser.sleep(BROWSER_PAUSE);
            }
        );

        it(`Once a User is LOGGED IN, should also be able to navigate, through the TOP MENU bar of buttons,`, () => {

        });

        it(`to '/' home route, by clicking on its #1 "${topMenuBtnsState[1].buttonName}" button`, () => {
            topMenu.btns().get(1).click().then(() => {
                expect(browser.getCurrentUrl()).toContain('/');
            });
            browser.sleep(BROWSER_PAUSE);
        });
        it(`and to '/profile' route, by clicking on its  #2 "${topMenuBtnsState[2].buttonName}" button`, () => {
            topMenu.btns().get(2).click().then(() => {
                expect(browser.getCurrentUrl()).toContain('/profile');
            });
            browser.sleep(BROWSER_PAUSE);
        });

        if (hasEmailVerified) {
            it(`and to '/todos' route, by clicking on its  #3 "${topMenuBtnsState[3].buttonName}" button`, () => {
                topMenu.btns().get(3).click().then(() => {
                    expect(browser.getCurrentUrl()).toContain('/todos');
                });
                browser.sleep(BROWSER_PAUSE);
            });

        } else {
            it(
                `but, as any loged in User WITHOUT the email verified, should NOT be able to visit App's Feature Module '/todos'` +
                `=> clicking on "${topMenuBtnsState[3].buttonName}" should re-directed to '/' home route`,
                () => {
                    topMenu.btns().get(3).click().then(() => {
                        expect(browser.getCurrentUrl()).toContain('/');
                    });
                    browser.sleep(BROWSER_PAUSE);
                }
            );
        }

        if (shallUserBeLoggedOut) {
            this.expectedUserToBeLoggedOut(signUpState, logOutBtn);
        }
    }

    expectedUserToBeLoggedOut(signUpState: any, logOutBtn: any): void {
        it(`'LOG OUT' button click, should LOG the User OUT of the APP, returning it to the left MENU login area`, () => {
            logOutBtn.click();
            browser.sleep(BROWSER_PAUSE);
        });
        it(
            `where AGAIN should be RENDERED an '${signUpState.titleTag}' tag supporting an welcome badge like "${signUpState.title}" ` +
            `and, underlyng, a "${signUpState.msg}" log in hint, on a '${signUpState.msgTag}' tag`
            , () => {
                this.expectText(signUpState.titleTag, signUpState.title);
                this.expectText(signUpState.msgTag, signUpState.msg);
            }
        );
    }

    /**
     * On the same way as upper 'expectedUserToHaveBeenLoggedIn()' logic.
     *
     * @param toSignUPstate -
     * @param toSignInBtn -
     */
    expectedUserToHaveBeenSignedUp(
        toSignUPstate: any,
        formInputs: Array<FormInputId>,
        invalidInputMsgs: any,
        afterSignInState: any,
    ): void {

        const inputedSignUpValues: Array<string> = ['Some FakeUser', 'FakeEmail@SomeFakeDomain.com', '123456789a'];

        it(`that should present an '${toSignUPstate.titleTag}' tag supporting an welcome badge like "${toSignUPstate.title}"`, () => {
            this.expectText(toSignUPstate.titleTag, toSignUPstate.title);
        });

        // MIND YOU:
        // Once 'Sign IN/UP' toggler button is clicked, the button gets TOGGLEd
        // => it's, now, a sign IN button (and not UP anymore) and vice-versa => different text!
        it(`that should toggle the Sign UP/IN toggler button text to "${toSignUPstate.buttonText}"`, () => {
            expect(this.getButtonByButtonText(toSignUPstate.buttonText).getText()).toBe(toSignUPstate.buttonText);
        });

        it(`and Login form should be INvalid on entering 'xyz' for both Name, Email and Password Inputs`, () => {
            const inputText = 'xyz';
            this.getInputById(formInputs[0].id).sendKeys(inputText);
            browser.sleep(BROWSER_PAUSE / 3);
            this.getInputById(formInputs[1].id).sendKeys(inputText);
            browser.sleep(BROWSER_PAUSE / 3);
            this.getInputById(formInputs[2].id).sendKeys(inputText);
            browser.sleep(BROWSER_PAUSE / 3);

            expect(this.getElementByCss('form').getAttribute('class')).toContain('ng-invalid');    // 'ng-touched ng-dirty ng-invalid'
        });

        it(`should present a NAME invalid input error msg "${invalidInputMsgs.name.minlength}"`, () =>
            this.expectedInvalidInputHTMLMsgFor('name', invalidInputMsgs.name.minlength)
        );
        it(`should present an EMAIL invalid input error msg "${invalidInputMsgs.email.email}"`, () =>
            this.expectedInvalidInputHTMLMsgFor('email', invalidInputMsgs.email.email)
        );
        it(
            `should present a PASSWORD invalid input error msg "` +
            `${invalidInputMsgs.password.pattern} ${invalidInputMsgs.password.minlength}` +
            `"`,
            () => {
                this.expectedInvalidInputHTMLMsgFor('password',
                    invalidInputMsgs.password.pattern + ' ' + invalidInputMsgs.password.minlength
                );
                browser.sleep(BROWSER_PAUSE);
            }
        );

        it(
            `However, should VALID credentials (for Name, Email and Password) like "` +
            inputedSignUpValues[0] + '", "' + inputedSignUpValues[1] + '", "' + inputedSignUpValues[2] + '"' +
            `be inputed on the |xyz-auth-form| form, followed by a SUBMIT click `,
            () => {
                for (let idx = 0; idx < 3; idx++) {
                    this.getInputById(formInputs[idx].id).clear();
                }

                inputedSignUpValues.forEach((inputFakeValue: string, idx: number) => {
                    this.getInputById(formInputs[idx].id).sendKeys(inputFakeValue);
                    browser.sleep(BROWSER_PAUSE / 3);
                });

                expect(this.getElementByCss('form').getAttribute('class')).toContain('ng-valid');    // 'ng-touched ng-dirty ng-invalid'
            }
        );

        it(`should be expected User has signed UP, and AUTOMATICALLY SIGNed IN, onto the Application`, () => {
            this.getElementByCss('xyz-submit-button button').click();
            browser.sleep(BROWSER_PAUSE);
        });

        // And the proof someone, in fact, has logged in, is the SAME for any kind of authoring (Google, Anonymously or mail/password)
        // EXCEPT if the User email is still NOT verified, on Firebase. If that is case, the difference is that
        // User should NOT have access to App's Feature Module TODOS, by the '/todos' Angular's Router route - 2nd param to 'false'
        this.expectedUserToHaveBeenLoggedIn(afterSignInState, false, true);
        // Upper Method call should do ALL the E2E tests, proofing User IS logged in, AND log him/her OUT, automatic and immediatelly
        // - 3rd param to 'true'

    }

}
