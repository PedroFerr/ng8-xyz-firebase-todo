import { ElementFinder } from 'protractor';

export interface BtnsNameState {
    buttonName: string;
    disabled: boolean;
}

export interface BtnsTextState {
    buttonText: string;
    disabled: boolean;
}

export interface BtnsClassState {
    buttonText: string;
    disabled: boolean;
}


export interface ImgSrcAndName {
    name: string;
    src: string;
}

export interface FormInputId {
    id: string;
    inputText: string;
}

type PromiseResolver = () => ElementFinder;

export interface TODOActionElements {
    create: {
        triggerTextCreation: PromiseResolver;
        submit: PromiseResolver;
        dismiss: PromiseResolver
    };
    edit: {
        greenStateTogglerToDone: PromiseResolver;
        redStateTogglerToTodo: PromiseResolver;
        triggerEditing: {
            triggerTextEdition: PromiseResolver;
            submit: PromiseResolver;
            dismiss: PromiseResolver;
        },
        delete: PromiseResolver;
    };
    write: {
        onTodoCreationTextArea: PromiseResolver;
        onTodoEditingTextArea: PromiseResolver;
    };
    filter: {
        toSeeOnlyTODOs: PromiseResolver;
        toSeeOnlyDONEs: PromiseResolver;
        toSeeThemAll: PromiseResolver;
    };
}
